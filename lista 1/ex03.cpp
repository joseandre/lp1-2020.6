#include <iostream>

using namespace std;

int busca_sequencial (int x, int tam){
    int aux, result;
    for (int i = 0; i < tam; i++)
    {
        cout << "Digite o elemento " << i << " da lista: ";
        cin >> aux;
        if (x == aux)
        {
            result = i;
        }
        
    }
    return result;

}
int main(){
    int x, tam, res;

    cout << endl << "Digite o número que deseja buscar: ";
    cin >> x;

    cout << endl << "Digite o tamanho da lista: ";
    cin >> tam;

    res = busca_sequencial(x, tam);

    cout << "O número desejado estava no elemento " << res << " da lista.";

    cout << "O número desejado  não estava na lista.";

}