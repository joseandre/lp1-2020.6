#include <iostream>

using namespace std;

int busca_sentinela(int lista[], int num, int tam){
    if (lista[tam - 1] == num){
        return tam - 1;
    }else{
        lista[tam - 1] = num;
    }

    int i = 0;
    while (lista[i] != num){
        i++;
    }

    if (i != (tam - 1)){
    return i;
    }

    return -1;
    
}

int main(){
    int num, tam, result;

    cout << "Digite o tamanho da lista: ";
    cin >> tam;

    int lista[tam];
    
    for (int i = 0; i < tam; i++)
    {
        cout << "Digite o valor do elemento " << i << ": ";
        cin >> lista[i];
    }
    
    cout << "Digite o valor que vc quer buscar na lista: ";
    cin >> num;

    result = busca_sentinela(lista, num, tam);

    if (result != -1)
    {
        cout << endl << "O valor " << num <<" estava na lista." << endl;
    }else
    {
        cout << endl << "O valor " << num <<" não contem na lista." << endl;
    }
    
    


 return 1;   
}