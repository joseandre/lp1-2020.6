#include <cstddef> // definição de NULL
#include<iostream>
#include "List.hpp"

using namespace std;

List::List(){
	head = NULL;
	fim = NULL;
}

List::~List(){
	while(head != NULL){
		Node* current = head;
		head = head->next;
		delete current;
	}
}

Node* List::getNode(int x){
	Node* ant = NULL;
	Node* current = head;
	while(current != NULL){
		if(current->value >= x){
			return ant;
		}
		ant = current;
		current = current->next;
	}
	return ant;
}

// 5
// 4 7 9
bool List::addFim(int x){

	Node* newNode = new Node(x);

	if (head == NULL)
	{
		head = newNode;
		fim = newNode;

	}else 
	{
		fim->next = newNode;
		fim = newNode;

	}

	return true;
}

bool List::addHead(int x){
	Node* newNode = new Node(x);

	if (head == NULL)
	{
		head = newNode;
		head->next = fim;

	}else 
	{
	newNode->next = head;
	head = newNode;
	}

	return true;
}

bool List::addInPosicao(int index, int value){
	int count = 0;
	Node* current = head;
	Node* ant;
	Node* newNode = new Node(value);

	if (index == 0)
	{
		cout << "AQUI IF" << endl;
		newNode->next = head;
		head = newNode;
		return true;
	}


	while (current != NULL)
	{
			cout << "AQUI WHILE " << endl;

		if (index == count)
		{
			ant->next = newNode;
			newNode->next = current;
			return true;
		}
		count += 1;
		ant = current;
		current = current->next;
	}
	
	cout << "[ERRO]   Não existe essa posição na lista. " << endl;
	
	return false;
}

int List::size(){
	int count = 0;
	Node* current = head;
	while(current != NULL){
		current = current->next;
		count += 1;
	}
	return count;
}

bool List::removePosicao(int index){
	if(head == NULL){
		cout << "[ERRO]   Não existe elementos na lista. " << endl;

		return false;
	} 

	int count = 0;
	Node* ant = NULL;
	Node* current = head;

	if (index == 0)
	{
		head = head->next;
		delete current;
		return 0;
	}
	
	while (current != NULL)
	{
		if (index == count)
		{
			ant->next = current->next;
			cout << "Elemento "<< current->value << "sendo removido da lista. " << endl;
			return true;
		}
		ant = current;
		current = current->next;
		count += 1;
	}
	
	return true;
}

bool List::removerHead(){
	if(head == NULL){
		cout << "[ERRO]   Não existe elementos na lista. " << endl;

		return false;
	}

	cout << "Elemento " << head->value << " removido da head." << endl;

	head = head->next;

	return true;
}

bool List::removeFim(){
	if(head == NULL){
		cout << "[ERRO]   Não existe elementos na lista. " << endl;

		return false;
	}

	cout << "Elemento " << fim->value << " removido da head." << endl;

	
	int count = 0;
	Node* current = head;

	while(current->next->next != NULL){
		current = current->next;
		count += 1;
	}

	fim = NULL;
	current->next = NULL;
	fim = current;

	return true;
}

int List::getToValue(int x){
	if(head == NULL){
		return -1;
	} else {
		int count = 0;
		Node* current = head;
		
		while(current != NULL){
			if(x == current->value){
				return count;
			}
			count += 1;
			current = current->next;
		}
		return -1;
	}
}

int List::getToIndex(int x){
	if(head == NULL){
		return -1;
	} else {
		int count = 0;
		Node* current = head;

		while(current != NULL){
			if(x == count){
				return current->value;
			}
			count += 1;
			current = current->next;
		}
		return -1;
	}
}							

void List::getElementos(){
	if(head == NULL){
		cout << "[ERRO]    a lista não possui elementos." << endl; 

		return;
	}else
	{
		Node* current = head;

		while (current->next != NULL)
		{
			cout << current->value << " ";

			current = current->next;
		}
		if (current->next == NULL && current->next != head)
		{
			cout << current->value << " ";
		}
		
	}
}