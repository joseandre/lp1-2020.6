
#ifndef LIST_HPP_
#define LIST_HPP_

#include "Node.hpp"

class List {
	Node* head;
	Node* fim;
	
	Node* getNode(int x);
	
	public:
	
		List();
		~List();
		
		// 5
		// 4 5 7 9
		bool addFim(int x);
		bool addHead(int x);
		bool addInPosicao(int x, int value);	//adiciona o elemento levando em consideração a sua posição, não o índice
		bool removePosicao(int x);
		bool removerHead();
		bool removeFim();
		int getToValue(int x);					//retorna o índice do valor na lista
		int getToIndex(int x);					//retorna o valor do índice na lista
		void getElementos();					//imprime todos os elementos da lista			
		int size();
};

#endif