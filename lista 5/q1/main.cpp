/*
    * As coisas que estão comentadas, podem ser descomentadas. 
    * Só comentei para não ficar tão bagunçado nas impressões no terminal.
*/

#include <iostream>
#include "List.hpp"

using namespace std;

int main (){

    List lista;

    /*lista.addHead(1); //adiciona um elemento na lista
	lista.addHead(2);
	lista.addHead(3);
	lista.addHead(4);
	lista.addHead(5);
	lista.addHead(6);*/

    lista.addFim(1); //adiciona um elemento na lista
	lista.addFim(2);
	lista.addFim(3);
	lista.addFim(4);
	lista.addFim(5);
	lista.addFim(6);

    lista.addInPosicao(2, 10);
	lista.addInPosicao(4, 11);
	lista.addInPosicao(0, 12);


    lista.getElementos();

    cout << endl << lista.getToIndex(2) << " index" << endl;

    cout << endl << lista.getToValue(2) << " value" << endl;

    //lista.removeFim();

   //lista.removerHead();

   lista.removePosicao(4);

    cout << endl;

    lista.getElementos();

    lista.~List();
    
	return 0;
}