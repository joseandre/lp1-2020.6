
#ifndef NODE_HPP_
#define NODE_HPP_


class Node {
	public:
		int value;
		Node * next;
		
		Node(int v);
};


#endif
