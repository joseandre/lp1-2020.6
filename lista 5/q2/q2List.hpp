
#ifndef LIST_HPP_
#define LIST_HPP_
#include "q2Node.hpp"

class List {
	Node* head;
	Node* fim;
		
	public:
	
		List();
		~List();
		
		// 5
		// 4 5 7 9
		bool addFim(int x);
		bool addInPosicao(int x, int value);	//adiciona o elemento levando em consideração a sua posição, não o índice
		int getToValue(int x);					//retorna o índice do valor na lista
		void getElementos();					//imprime todos os elementos da lista			
		int size();
};

#endif