#include <cstddef> // definição de NULL
#include<iostream>
#include "q2List.hpp"

using namespace std;

List::List(){
	head = NULL;
	fim = NULL;
}

List::~List(){
	while(head != NULL){
		Node* current = head;
		head = head->next;
		delete current;
	}
}



// 5
// 4 7 9
bool List::addFim(int x){

	Node* newNode = new Node(x);


	if (head == NULL)
	{

		head = newNode;
		head->next = fim;
		fim = newNode;
		fim->next = head;

	}else 
	{

		fim->next = newNode;
		fim = newNode;
		fim->next = head;

	}

	return true;
}

bool List::addInPosicao(int index, int value){
	int count = 0;
	Node* current = head;
	Node* ant;
	Node* newNode = new Node(value);

	if (index == 0)
	{
		cout << "AQUI IF" << endl;
		newNode->next = head;
		head = newNode;
		return true;
	}


	while (current != head)
	{
			cout << "AQUI WHILE " << endl;

		if (index == count)
		{
			ant->next = newNode;
			newNode->next = current;
			return true;
		}
		count += 1;
		ant = current;
		current = current->next;
	}
	
	cout << "[ERRO]   Não existe essa posição na lista. " << endl;
	
	return false;
}

int List::size(){
	int count = 0;
	Node* current = head;
	while(current != head){
		current = current->next;
		count += 1;
	}
	return count;
}

int List::getToValue(int x){
	if(head == NULL){
		return -1;
	} else {
		int count = 0;
		Node* current = head;
		
		while(current != head){
			if(x == current->value){
				return count;
			}
			count += 1;
			current = current->next;
		}
		return -1;
	}
}
			
void List::getElementos(){
	if(head == NULL){

		cout << "[ERRO]    a lista não possui elementos." << endl; 

		return;

	}else
	{
		//int count = 0;
		Node* current = head;

		while (current->next != head)
		{
			cout << current->value << " ";

			current = current->next;
		}
		if (current->next == head /*&& current->next != head*/)
		{
			cout << current->value << " ";
		}
		
	}
	return;
}