#ifndef Q1RESTO_HPP
#define Q1RESTO_HPP

#include <iostream>

class Resto {
	
	int * tabela;						// tabela inicial
	int tamanho;						// armazena a capacidade da lista

    int gerarChave(int num);			// método que gera a chave (posição)

	public:	
		Resto(int tam);					// construtor da classe
		~Resto();						// destrutor da classe
		
		void set(int num);				// método que passa os elementos para a tabela dispersa 
		void get(int num);				// recupera um elemento na tabela dispersa
};

#endif