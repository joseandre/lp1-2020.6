/*
    *   Eu recomendaria a função de dobra, pelo fato do código ser restringido ao método de encadeamento interno. 
    *   Só consegui fazer a implementação da função utilizando o resto para fazer o mapeamento, tive dificuldade para elaborar uma maneira 
    * de aplicar a dobra em código, apesar de ter entendido bem o conceito, não consegui fazer a implementação. O método de multiplicação 
    * eu confeço que nem tentei fazer e ir atrás de fontes para estudar e conseguir fazer, optei por ir fazer as outras questões e se sobrasse
    * tempo, iria voltar para ela.
    *   Criei um método para adicionar o elemento na lista, criei um método para recuperar o elemento que foi passado por parâmetro, caso
    * ele faça parte da lista, e criei um método para gerar a chave de mapeamento.
    *   Comentei as funcionalidades do métodos e atributos no arquivo .hpp.
 */

#include <iostream>
#include <cstdlib>
#include "resto.hpp"

using namespace std;

int main(){

    Resto resto(100);

    int seed = 962;
    srand(seed);

    int values [100];

    for(int i=0; i < 100; i++){
        values[i] = rand() % 1000;
        resto.set(values[i]);
        
    }

    cout << endl;

    resto.get(922);
    resto.get(522);
    resto.get(486);
    resto.get(554);

    cout << endl;

    return 0;
}