#include "resto.hpp"
#include <iostream>

using namespace std;

Resto::Resto(int tam){
    tamanho = tam;
    tabela = new int (tam);    

    for (int i = 0; i < tam; i++)
    {
        tabela[i] = -1;
    }
    
}

Resto::~Resto(){
    delete [] tabela;
}

void Resto::set(int num){
    int posicao;
    bool aux = false;
    posicao = gerarChave(num);
    int posicao_aux = posicao;

    while (aux == false)
    {
        if (tabela[posicao_aux] == -1)
        {
            tabela[posicao_aux] = num;
            cout << endl << "O número " << tabela[posicao_aux] << " foi guardado na posição: " << posicao_aux << endl;

            aux = true;
                
        }else if (posicao_aux == tamanho-1)
        {
            posicao_aux = 0;

        }else if (posicao_aux+1 == posicao)
        {
            cout << endl << "O número " << num << " NÃO foi guardado! " << endl;

            return;
        }else
        {
            posicao_aux++;
        }
        
    }
    
}

void Resto::get(int num){

    int posicao = gerarChave(num);

    for (int i = posicao; i < tamanho; i++)
    {
        if (tabela[i] == num)
        {
            cout << endl << "O número " << num << " está na posição: " << i << endl;
            return;
        }else if (i == tamanho-1)
        {
            i = -1;
        }else if (i+1 == posicao)
        {
            cout << endl << "O número " << num << " NÃO está na tabela! " << endl;
            return;
        }
        
    }
    
}

int Resto::gerarChave(int num){
    return num % tamanho;
}


