/*
    *   Fiz um algoritmo que cria dois conjuntos de valores naturais e utilizam os métodos das classes para fazerem operações de intersecção 
    * e união entre eles.
    *   Criei métodos para armazenar ele elementos, fiz uso do mapeamento utilizando a função de resto, criei um método para gerar 
    * a chave de mapeamento, criei um método para recuperar o elemento na tabela, caso ele pertença ao conjunto, criei um método para 
    * retornar o elemento que contém na posição que foi passada como parâmentro, criei um método que retorna a capacidade  do conjunto, 
    * e criei os métodos de união e intersecção, ambos retornam um conjunto com o tipo Resto, que é a classe que foi utilizada no algoritmo.
    *   Comentei a função de cada método e atributo no arquivo .hpp
    *   OBSERVAÇÃO: instanciei de modo estático pois estava cansado de ter que utilizar a entrada dos valores dos conjuntos pelo teclado, 
    * mas basta descomentar e comentar a parte estática para fazer uso.
    *   OBSERVAÇÃO 2: em algumas vezes eu tive que executar duas vezes o mesmo conjunto para dar certo o que o algoritmo deve entregar, 
    * creio que seja BUG do meu PC, pois eu não achei o que estaria causando o erro de conter lixo nos conjuntos, inves dos elementos.
*/

#include <iostream>
#include <cstdlib>
#include "resto.hpp"

using namespace std;

int main(){

   /* int tamanho, aux;

    cout << endl << "Digite o tamanho do conjunto A: ";
    cin >> tamanho;

    Resto a(tamanho);

    for (int i = 0; i < tamanho; i++)
    {
        cout << endl <<  "Digite o elemento " << i+1 << ": ";
        cin >> aux;
        a.set(aux);
    }

    cout << endl << "Digite o tamanho do conjunto B: ";
    cin >> tamanho;

    Resto b(tamanho);

    for (int i = 0; i < tamanho; i++)
    {
        cout << endl << "Digite o elemento " << i+1 << ": ";
        cin >> aux;
        b.set(aux);
    }*/

    Resto a(5);
    Resto b(6);

    cout << endl << endl;

    a.set(4);
    a.set(8);
    a.set(12);
    a.set(16);
    a.set(20);

    cout << endl << endl;

    b.set(2);
    b.set(4);
    b.set(6);
    b.set(8);
    b.set(10);
    b.set(12);

    cout << endl << endl;


    Resto c = a.interseccao(b);
    cout << endl << "Os elementos de C são: ";
    for (int i = 0; i < c.capacidade(); i++)
    {
        cout << c.getPosicao(i) << " ";
    }
    cout << endl << endl;


    Resto d = a.uniao(b);
    cout << endl << "Os elementos de D são: ";
    for (int i = 0; i < d.capacidade(); i++)
    {
        cout << d.getPosicao(i) << " ";
    }
    cout << endl << endl;

    return 0;
}