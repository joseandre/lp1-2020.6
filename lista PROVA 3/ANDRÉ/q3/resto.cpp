#include "resto.hpp"
#include <iostream>

using namespace std;

Resto::Resto(){
    tabela = new int; 
}

Resto::Resto(int tam){
    tamanho = tam;
    tabela = new int (tam);    

    for (int i = 0; i < tam; i++)
    {
        tabela[i] = -1;
    }
    
}

Resto::~Resto(){
    delete [] tabela;
}

void Resto::set(int num){
    int posicao;
    bool aux = false;
    posicao = gerarChave(num);
    int posicao_aux = posicao;

    while (aux == false)
    {
        if (tabela[posicao_aux] == -1)
        {
            tabela[posicao_aux] = num;
            cout << "O número " << tabela[posicao_aux] << " foi guardado na posição: " << posicao_aux << endl;

            aux = true;
                
        }else if (posicao_aux == tamanho-1)
        {
            posicao_aux = 0;

        }else if (posicao_aux+1 == posicao)
        {
            cout << endl << "O número " << num << " NÃO foi guardado! " << endl;

            return;
        }else
        {
            posicao_aux++;
        }
        
    }
    
}

void Resto::get(int num){

    int posicao = gerarChave(num);

    for (int i = posicao; i < tamanho; i++)
    {
        if (tabela[i] == num)
        {
            cout << endl << "O número " << num << " está na posição: " << i << endl;
            return;
        }else if (i == tamanho-1)
        {
            i = -1;
        }else if (i+1 == posicao)
        {
            cout << endl << "O número " << num << " NÃO está na tabela! " << endl;
            return;
        }
        
    }
    
}

int Resto::getPosicao(int posicao){
    if (posicao < tamanho)
    {
        return tabela[posicao];
    }
    cout << "A posicao não existe!!" << endl;
    return -1;
}

int Resto::gerarChave(int num){
    return num % tamanho;
}

int Resto::capacidade(){
    return tamanho;
}

Resto Resto::interseccao(Resto b){
    int *vet = new int;
    int count = 0;

    for (int i = 0; i < tamanho; i++)
    {
        for (int j = 0; j < b.capacidade(); j++)
        {
            if (tabela[i] == b.getPosicao(j))
            {   
                bool contem = false;

                for (int k = 0; k < count; k++)
                {
                    if (tabela[i] == vet[k])
                    {
                        contem = true;
                    }
                    
                }
                if (contem == false)
                {
                    vet[count] = tabela[i];
                    count++;
                }
                
            }
            
        }
        
    }
    Resto c(count);
    
    for (int i = 0; i < count; i++)
    {
        c.set(vet[i]);
    }
    return c;
}

Resto Resto::uniao(Resto b){
    int *vet = new int;
    int count = 0;


    for (int i = 0; i < tamanho; i++)
    {
        vet[i] = tabela[i];
        count++;
    }
    

    for (int i = 0; i < b.capacidade(); i++)
    {
        bool contem = false;
        
        for (int j = 0; j < count; j++)
        {
            if (b.getPosicao(i) == vet[j])
            {
                contem = true;
                break;
            }
            
        }
        if (contem == false)
        {
            vet[count] = b.getPosicao(i);
            count++;
        }
    }

    Resto d(count);
    
    for (int i = 0; i < count; i++)
    {
        d.set(vet[i]);
    }
    
    return d;
}