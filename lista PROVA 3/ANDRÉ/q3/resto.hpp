#ifndef Q2RESTO_HPP
#define Q2RESTO_HPP

#include <iostream>

class Resto {
	
	int * tabela;						// tabela inicial
	int tamanho;						// atributo que armazena a capacidade da lista

    int gerarChave(int num);			// método que gera a chave (posição)

	public:	
		Resto(int tam);					// construtor da classe
		Resto();						// construtor da classe
		~Resto();						// destrutor da classe
		
		void set(int num);				// método que passa os elementos para a tabela dispersa 
		void get(int num);				// recupera um elemento na tabela dispersa
		int getPosicao(int posicao);	// retorna o elemento que tem na posição que foi passado por parâmetro
		int capacidade();
		Resto interseccao(Resto b);
		Resto uniao(Resto b);
};

#endif