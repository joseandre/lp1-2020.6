
#ifndef NODE_H_
#define NODE_H_


class Node {
	public:
		int value;
		Node * next;
		Node(int v);
};


#endif
