#include "CircularList.hpp"

int main()
{
    CircularList list;

    list.listSize();
    list.addNode(10);
    list.addNode(20);
    list.addNode(30);
    list.addNode(40);
    list.addNode(50);
    list.addNode(60);

    cout << "Tamanho da lista: " << list.listSize() << endl;
    list.printList();

    cout << "Digite um elemento que deseja procurar: ";
    int elemento;
    cin >> elemento;

    if (list.findNode(elemento) == elemento)
        cout
            << "Elemento encontrado!" << endl;
    else
        cout << "Elemento não encontrado!" << endl;

    return 0;
}