
#include "List.hpp"

List::List()
{
    head = nullptr;
    tam = 0;
}

List::~List()
{
    while (head != nullptr)
    {
        Node *current = head;
        head = head->next;
        delete current;
    }
}

Node *List::getNode(int pos)
{
    Node *ant = nullptr;
    Node *current = head;
    int count = 0;
    while (current != nullptr)
    {
        if (count >= pos)
        {
            return ant;
        }
        count += 1;
        ant = current;
        current = current->next;
    }
    return ant;
}

bool List::add(int x, int pos)
{

    Node *newNode = new Node(x);
    newNode->next = nullptr;
    newNode->previous = nullptr;

    if (pos > tam)
    {
        cout << "A Lista não possui tamanho para inserir esse elemento." << endl;
        return false;
    }

    Node *current = head;
    for (int i = 0; i < pos; i++)
        current = current->next;

    if (current != nullptr)
    {

        newNode->next = current->next;
        newNode->previous = current;
        current->next = newNode;
        if (newNode->next != nullptr)
            newNode->next->previous = newNode;
        return true;
    }
    return false;
}

bool List::remove(int pos)
{
    if (pos > size())
    {
        cout << "Essa posição não existe na lista!!" << endl;
        return false;
    }
    Node *current = head;

    for (int i = 0; current != NULL && i < pos; i++)
        current = current->next;

    if (head == NULL || current == NULL)
        return true;

    if (current->next != NULL)
        current->next->previous = current->previous;

    if (current->previous != NULL)
        current->previous->next = current->next;

    delete current;
    return true;
}

int List::size()
{
    int count = 0;
    Node *current = head;
    while (current != nullptr)
    {
        current = current->next;
        count += 1;
    }
    return count;
}

int List::get(int x)
{
    if (head == nullptr)
    {
        return -1;
    }
    else
    {
        int count = 0;
        Node *current = head;
        while (current != nullptr)
        {
            if (x == current->value)
            {
                return count;
            }
            count += 1;
            current = current->next;
        }
        return -1;
    }
}