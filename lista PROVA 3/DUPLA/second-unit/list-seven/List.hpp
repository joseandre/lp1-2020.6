
#ifndef LIST_H_
#define LIST_H_

#include "Node.hpp"
#include <iostream>

using namespace std;

class List
{
    Node *head;
    Node *getNode(int x);
    int tam;

public:
    List();
    ~List();

    bool add(int x, int pos);
    bool remove(int pos);
    int get(int x);
    int size();
};

#endif