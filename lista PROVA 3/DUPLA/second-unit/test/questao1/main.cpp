/* 
Para a resolução dessa questão foi usada as noções de alocação encadeada aplicando em uma pilha. Os métodos que são mais complexos são os de 
adição e o de remoção. Para o primeiro é usado a lógica de uma adição em pilha, só que com a alocação dinâmica, para a remoção é a mesma coisa.

Os demais métodos (print, get, size, isEmpty) foram retirados de questões anteriores. 
*/

#include "stack.hpp"

int main()
{
     Stack stack, stack2, stack3, stack4, stack5;

     // Exemplo 1
     stack.add(1);
     stack.add(2);
     stack.add(3);
     stack.add(4);
     stack.add(5);
     stack.print();
     stack.remove();
     stack.remove();
     stack.remove();
     stack.remove();
     stack.remove();
     stack.get();  // Verificando elemento do topo
     stack.size(); // Verificando tamanho da pilha
     stack.add(10);
     stack.remove();
     stack.add(20);
     stack.remove();
     stack.add(30);
     stack.add(40);
     stack.remove();
     stack.remove();
     stack.get();
     stack.size();
     stack.add(50);
     stack.print();
     stack.~Stack();

     cout << endl
          << "Começando exemplo 2" << endl
          << endl;

     // Exemplo 2
     stack2.add(1);
     stack2.remove();
     stack2.add(2);
     stack2.remove();
     stack2.size();
     stack2.get();
     stack2.add(3);
     stack2.add(4);
     stack2.add(5);
     stack2.add(6);
     stack2.add(7);
     stack2.print();
     stack2.remove();
     stack2.remove();
     stack2.remove();
     stack2.add(8);
     stack2.get();
     stack2.add(9);
     stack2.get();
     stack2.add(10);
     stack2.print();
     stack2.remove();
     stack2.remove();
     stack2.print();
     stack2.size();
     stack2.remove();
     stack2.print();
     stack2.size();
     stack2.remove();
     stack2.remove();
     stack2.size();
     stack2.print();
     stack2.~Stack();

     // Exemplo 3
     cout << endl
          << "Começando exemplo 3" << endl
          << endl;

     stack3.get();
     stack3.add(1);
     stack3.add(2);
     stack3.add(3);
     stack3.size();
     stack3.print();
     stack3.add(4);
     stack3.add(5);
     stack3.add(6);
     stack3.add(7);
     stack3.size();
     stack3.print();
     stack3.remove();
     stack3.remove();
     stack3.remove();
     stack3.remove();
     stack3.remove();
     stack3.remove();
     stack3.get();
     stack3.remove();
     stack3.size();
     stack3.add(2);
     stack3.add(8);
     stack3.print();
     stack3.remove();
     stack3.add(9);
     stack3.add(10);
     stack3.print();
     stack3.remove();
     stack3.remove();
     stack3.get();
     stack3.~Stack();

     // Exemplo 4
     cout << endl
          << "Começando exemplo 4" << endl
          << endl;

     stack4.add(1);
     stack4.remove();
     stack4.get();
     stack4.size();
     stack4.print();
     stack4.add(2);
     stack4.remove();
     stack4.print();
     stack4.add(3);
     stack4.add(4);
     stack4.remove();
     stack4.add(5);
     stack4.remove();
     stack4.add(6);
     stack4.print();
     stack4.size();
     stack4.get();
     stack4.add(7);
     stack4.add(8);
     stack4.add(9);
     stack4.print();
     stack4.size();
     stack4.get();
     stack4.remove();
     stack4.remove();
     stack4.get();
     stack4.add(10);
     stack4.size();
     stack4.remove();
     stack4.print();
     stack4.get();
     stack4.~Stack();

     // Exemplo 5
     cout << endl
          << "Começando exemplo 5" << endl
          << endl;

     stack5.add(1);
     stack5.print();
     stack5.add(2);
     stack5.print();
     stack5.remove();
     stack5.add(3);
     stack5.remove();
     stack5.remove();
     stack5.get();
     stack5.size();
     stack5.print();
     stack5.add(4);
     stack5.add(5);
     stack5.get();
     stack5.add(6);
     stack5.add(7);
     stack5.get();
     stack5.size();
     stack5.get();
     stack5.print();
     stack5.remove();
     stack5.remove();
     stack5.remove();
     stack5.size();
     stack5.get();
     stack5.remove();
     stack5.print();
     stack5.add(8);
     stack5.add(9);
     stack5.remove();
     stack5.add(10);
     stack5.remove();
     stack5.size();
     stack5.get();
     stack5.remove();
     stack5.size();
     stack5.get();
     stack5.~Stack();
}