/* 
A lógica principal dessa questão estão nos métodos que foram feitos. Esses são os de adicionar, na qual adiciono os elementos no início (addFront) e no final (addLast) da lista duplamente ligada
os de remover, onde removo os elementos do inicio (deleteFront) e os do final (deleteLast) e por fim os métodos para retornar os elementos, um para retornar o elemento do início (getFront)
e outro para o final (getLast).

Ainda existem outros métodos como retornar o tamanho da lista e outro apenas para mostrar os elementos que tenho em lista.

 */

#include "List.hpp"

int main()
{
    List list, list2, list3;

    // Exemplo 1
    list.addFront(1);
    list.addLast(2);
    list.addFront(3);
    list.addLast(4);
    list.print(); /* 3 1 2 4  */

    list.deleteLast();
    list.deleteFront();
    list.print(); /* 1 2 */

    list.addFront(5);
    list.addFront(6);
    list.deleteFront();
    list.addFront(7);
    list.print(); /* 7 5 1 2 */

    list.addLast(8);
    list.addLast(9);
    list.deleteLast();
    list.addLast(10);
    list.print(); /* 7 5 1 2 8 10 */

    list.deleteFront();
    list.deleteFront();
    list.deleteFront();
    list.deleteFront();
    list.deleteFront();
    list.deleteFront();
    list.print(); /* Lista Vázia */
    list.addFront(1);
    list.addFront(2);
    list.addFront(3);
    list.addFront(4);
    list.addFront(5);
    list.getFront(); /* 5 */
    list.print();    /* 5 4 3 2 1 */

    list.deleteLast();
    list.deleteLast();
    list.deleteLast();
    list.deleteLast();
    list.deleteLast();
    list.print(); /* Lista Vázia */
    list.addFront(1);
    list.deleteFront();
    list.print(); /* Lista Vázia */

    list.addFront(1);
    list.addFront(2);
    list.print(); /* 2 1 */
    list.deleteFront();
    list.deleteFront();
    list.print(); /* Lista Vázia */

    list.addLast(1);
    list.addLast(2);
    list.addLast(3);
    list.print();   /* 1 2 3 */
    list.getLast(); /* 3 */

    cout << "Tamanho da lista: " << list.size() << endl;

    list.~List();

    // Exemplo 2
    cout << "\n\nIniciando exemplo 2\n\n\n";

    cout << "Tamanho da lista: " << list2.size() << endl;
    list2.addFront(1);
    list2.deleteFront();
    list2.addLast(2);
    list2.deleteLast();
    cout << "Tamanho da lista: " << list2.size() << endl;

    list2.addLast(1);
    list2.addLast(2);
    list2.addLast(3);
    list2.print(); /* 1 2 3 */
    cout << "Tamanho da lista: " << list2.size() << endl;
    list2.deleteLast();
    list2.deleteLast();
    list2.deleteLast();

    list2.addFront(1);
    list2.addFront(2);
    list2.addFront(3);
    list2.print(); /* 3 2 1 */
    cout << "Tamanho da lista: " << list2.size() << endl;

    list2.deleteLast();
    list2.deleteLast();
    list2.deleteLast();
    cout << "Tamanho da lista: " << list2.size() << endl;
    list2.addLast(1);
    list2.addLast(2);
    list2.addLast(3);
    list2.print(); /* 1 2 3 */
    cout << "Tamanho da lista: " << list2.size() << endl;

    list2.deleteFront();
    list2.deleteFront();
    list2.deleteFront();
    cout << "Tamanho da lista: " << list2.size() << endl;
    list2.addFront(1);
    list2.addFront(2);
    list2.addFront(3);
    list2.print(); /* 3 2 1 */
    list2.deleteFront();
    list2.deleteFront();
    list2.deleteFront();

    cout << "Tamanho da lista: " << list2.size() << endl;
    list.getFront(); /* 1 */
    list2.addLast(1);
    list2.addLast(2);
    list2.addLast(3);
    list2.addLast(4);
    list2.print(); /* 1 2 3 4 */
    list2.deleteLast();
    list2.deleteLast();
    list2.deleteLast();
    list2.print(); /* 1 */
    list2.addLast(5);
    list2.addLast(6);
    list2.deleteLast();
    list2.print(); /* 1 5 */
    list2.addLast(7);
    list2.addLast(8);
    list2.deleteLast();
    list2.print(); /* 1 5 7  */
    cout << "Tamanho da lista: " << list2.size() << endl;
    list2.deleteLast();
    list2.getLast(); /* 5 */
    list2.deleteLast();
    list2.deleteLast();
    cout << "Tamanho da lista: " << list2.size() << endl;

    list2.addFront(1);
    list2.addFront(2);
    list2.addFront(3);
    list2.print(); /* 3 2 1  */
    list2.deleteFront();
    list2.getLast(); /* 1 */
    list2.addFront(4);
    list2.addFront(5);
    list2.deleteFront();
    list2.addFront(6);
    list2.print(); /* 6 4 2 1  */
    list2.deleteFront();
    list2.addFront(7);
    list2.addFront(8);
    list2.getFront(); /* 8 */
    list2.addFront(9);
    list2.deleteFront();
    list2.deleteFront();
    list2.print();
    list2.addFront(10);
    list2.print();

    cout << "Tamanho da lista: " << list2.size() << endl;
    list2.~List();

    // Exemplo 3
    cout << "\n\nIniciando exemplo 3\n\n\n";

    cout << "Tamanho da lista: " << list3.size() << endl;
    list3.addFront(1);
    list3.addFront(2);
    list3.addFront(3);
    list3.addFront(4);
    list3.print(); /* 4 3 2 1 */
    list3.addFront(5);
    list3.addFront(6);
    list3.deleteFront();
    list3.deleteFront();
    list3.deleteFront();
    list3.deleteFront();
    list3.addLast(7);
    list3.addLast(8);
    list3.addLast(9);
    list3.addLast(10);
    list3.print(); /* 2 1 7 8 9 10 */
    cout << "Tamanho da lista: " << list3.size() << endl;
    list3.deleteLast();
    list3.deleteLast();
    list3.deleteLast();
    cout << "Tamanho da lista: " << list3.size() << endl;
    list3.print(); /* 2 1 7  */

    list3.getFront();
    list3.addFront(1);
    list3.deleteFront();
    list3.deleteFront();
    list3.deleteFront();
    list3.addFront(3);
    list3.addFront(4);
    list3.addFront(5);
    list3.deleteFront();
    list3.addFront(6);
    list3.deleteFront();
    cout << "Tamanho da lista: " << list3.size() << endl;
    list3.print(); /* 4 3 7  */

    list3.deleteLast();
    list3.deleteLast();
    list3.deleteLast();
    list3.addLast(1);
    list3.addLast(2);
    list3.addLast(3);
    list3.deleteLast();
    list3.addLast(4);
    list3.addLast(5);
    list3.deleteLast();
    list3.deleteLast();
    list3.deleteLast();
    list3.addLast(6);
    list3.getLast();
    list3.print();
    cout << "Tamanho da lista: " << list3.size() << endl;

    list3.deleteFront();
    list3.deleteFront();
    cout << "Tamanho da lista: " << list3.size() << endl;
    list3.addFront(1);
    list3.addFront(2);
    list3.addFront(3);
    list3.addFront(4);
    list3.addFront(5);
    list3.addFront(6);
    list3.addFront(7);
    list3.print(); /* 7 6 5 4 3 2 1   */
    list3.getLast();
    list3.getFront();
    cout << "Tamanho da lista: " << list3.size() << endl;

    list3.deleteLast();
    list3.deleteLast();
    list3.deleteLast();
    list3.deleteLast();
    list3.deleteLast();
    list3.deleteLast();
    list3.deleteLast();
    cout << "Tamanho da lista: " << list3.size() << endl;
    list3.addLast(1);
    list3.addLast(2);
    list3.addLast(3);
    list3.addLast(4);
    list3.getLast();
    list3.getFront();
    cout << "Tamanho da lista: " << list3.size() << endl;
    list3.print(); /* 1 2 3 4 */

    list3.deleteLast();
    list3.deleteLast();
    list3.deleteLast();
    list3.deleteLast();
    list3.addFront(1);
    list3.addFront(2);
    list3.addFront(3);
    list3.addFront(4);
    list3.getFront();
    cout << "Tamanho da lista: " << list3.size() << endl;
    list3.print(); /* 4 3 2 1  */

    list3.deleteFront();
    list3.deleteFront();
    list3.deleteFront();
    list3.deleteFront();
    list3.addLast(5);
    list3.addLast(6);
    list3.addLast(7);
    list3.addLast(8);
    list3.addLast(9);
    list3.addLast(10);
    cout << "Tamanho da lista: " << list3.size() << endl;
    list3.print(); /* 5 6 7 8 9 10  */
    list3.getFront();
    list3.getLast();

    list3.~List();
}