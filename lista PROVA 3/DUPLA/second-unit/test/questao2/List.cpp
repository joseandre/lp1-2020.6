
#include "List.hpp"
#include <iostream>
using namespace std;

List::List()
{
	head = nullptr;
}

List::~List()
{
	while (head != nullptr)
	{
		Node *current = head;
		head = head->next;
		delete current;
	}
}

void List::addFront(int value)
{
	Node *aux = new Node(value);
	aux->next = head;
	aux->previous = nullptr;

	if (head != nullptr)
		head->previous = aux;

	head = aux;
}

void List::addLast(int value)
{
	Node *newNode = new Node(value);
	Node *last = head;

	if (size() == 0)
	{

		newNode->previous = nullptr;
		head = newNode;
		return;
	}

	while (last->next != nullptr)
		last = last->next;

	last->next = newNode;

	newNode->previous = last;
}

int List::deleteFront()
{
	if (head != nullptr)
	{
		Node *aux = head;
		head = head->next;
		delete aux;

		if (head != nullptr)
			head->previous = nullptr;
	}
	return true;
}

int List::deleteLast()
{
	if (head != nullptr)
	{
		if (head->next == nullptr)
			head = nullptr;
		else
		{

			Node *aux = head;
			while (aux->next->next != nullptr)
				aux = aux->next;

			Node *lastNode = aux->next;
			aux->next = nullptr;
			delete lastNode;
		}
	}
	return true;
}

int List::size()
{
	int count = 0;
	Node *current = head;
	while (current != nullptr)
	{
		current = current->next;
		count += 1;
	}
	return count;
}

int List::getFront()
{

	if (head == NULL)
	{
		cout << "Lista Vázia!" << endl;
		return -1;
	}
	else
		cout << "Elemento inicial: " << head->value << endl;
	return head->value;
}

int List::getLast()
{
	Node *aux = head;
	if (head != nullptr)
	{
		if (head->next == nullptr)
			head = nullptr;
		else
		{

			while (aux->next != nullptr)
				aux = aux->next;
		}
	}
	cout << "Elemento final: " << aux->value << endl;
	return aux->value;
}

void List::print()
{
	Node *h = head;
	if (head == NULL)
		cout << "Lista vázia!" << endl;
	else
	{
		while (h != NULL)
		{
			if (h->next == NULL)
			{
				cout << h->value << " ";
			}
			else
				cout << h->value << "-->";
			h = h->next;
		}
	}
	cout << endl;
}