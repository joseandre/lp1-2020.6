#include <iostream>

using namespace std;

int main() {
  int n, x, aux, temp = 100;
  
  cout << "Digite um inteiro (x): ";
  cin >> x;

  cout << "Digite o tamanho que deseja para seu vetor: ";
  cin >> n;

  for(int i = 0; i < n ; i++) {
    cout << "Digite um elemento: ";
    cin >> aux;

    if(aux == x) {
      temp = i;
    }
  }

  if(temp != 100) cout << temp << endl;
  else  cout << "-1" << endl;

}
