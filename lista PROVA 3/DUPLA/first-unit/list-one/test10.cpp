#include <iostream>

using namespace std;

int procurarElemento(int vetor[], int ini, int fim);

int main() {
  int n, x;

  cout << "Defina o tamanho de sua sequência: ";
  cin >> n;

  int s[n];
  
  for(int i = 0; i < n - 1 ; i++) {
    cin >> s[i];  
  }
  x = procurarElemento(s, 0, n-1);
  cout << x  << endl;
}

int procurarElemento(int vetor[], int ini, int fim) {
  if(ini > fim) {
    return ini;
  }

  int meio = ini + (fim - ini) / 2;

  if(vetor[meio] == meio) return procurarElemento(vetor,  meio + 1, fim );
  else return procurarElemento(vetor, ini, meio - 1);
}

