#include <iostream>

using namespace std;

int buscaBinariaRecursiva(int vetor[], int inicio, int fim , int chave);

int main() {
  int n, x;

  cout << "Digite a quantidade de elementos de sua sequência: ";
  cin >> n;

  int s[n];

 cout << "Digite " << n << " elementos de forma ordenada." << endl; 
  for(int i = 0; i < n ; i++) {
    cin >> s[i];
  }
  cout << "Digite o valor para x: ";
  cin >> x;
  
  cout << buscaBinariaRecursiva(s, 0, n-1, x) << endl;
}


int buscaBinariaRecursiva(int vetor[], int inicio, int fim , int chave) {
  if(fim >= inicio) {
    int meio = inicio + (fim - inicio) / 2;

    if(vetor[meio] == chave) return meio;

    if(vetor[meio] > chave) return buscaBinariaRecursiva(vetor, inicio, fim - 1, chave);

    return buscaBinariaRecursiva(vetor, meio + 1, fim, chave);
  }

  return -1;
}
