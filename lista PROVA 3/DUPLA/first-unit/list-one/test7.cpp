#include <iostream>

using namespace std;

int buscaBinaria(int vetor[], int tam, int chave);

int main() {
  int n, x;

  cout << "Digite a quantidade de elementos de sua sequência: ";
  cin >> n;

  int s[n];

 cout << "Digite " << n << " elementos de forma ordenada." << endl; 
  for(int i = 0; i < n ; i++) {
    cin >> s[i];
  }
  cout << "Digite o valor para x: ";
  cin >> x;
  
  cout << buscaBinaria(s, n, x) << endl;
}


int buscaBinaria(int vetor[], int tam, int chave) {
  int limInferior = 0;
  int limSuperior = tam-1;
  int meio;

  while(limInferior <= limSuperior) {
    meio = (limInferior + limSuperior) / 2;
    if(chave == vetor[meio]) return meio;
    if(chave < vetor[meio]) limSuperior = meio -1;
    else limInferior = meio + 1;
  }
 return -1 ;
}
