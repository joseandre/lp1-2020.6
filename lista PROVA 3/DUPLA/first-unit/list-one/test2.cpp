#include <iostream>

using namespace std;

int buscaSequencialSentinela(int vetor[], int tam, int chave);

int main() {
  int n, x;
  
  cout << "Digite um inteiro: ";
  cin >> n;

  int s[n];

  for(int i = 0; i < n ; i++) {
    cout << "Número " << i+1 << ": " ;
    cin >> s[i];
  }

  cin >> x;

  cout <<  buscaSequencialSentinela(s, n, x) << endl;
}

int buscaSequencialSentinela(int vetor[], int tam, int chave) {
  int i = 0;
  vetor[tam] = chave;
  while(vetor[i] != chave) {
    i++;
  }
  if(i < tam) return i;
  return -1;

}

