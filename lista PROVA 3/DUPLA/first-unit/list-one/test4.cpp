#include <iostream>

using namespace std;

int buscaSequencialRecursiva(int vetor[], int tam, int chave);

int main() {
  int n, x;
  
  cout << "Digite um inteiro: ";
  cin >> n;

  int s[n];

  for(int i = 0; i < n ; i++) {
    cout << "Número " << i+1 << ": " ;
    cin >> s[i];
  }

  cin >> x;

  cout <<  buscaSequencialRecursiva(s, n, x) << endl;
}

int buscaSequencialRecursiva(int vetor[], int tam, int chave) {
    if(tam == 0) return -1;
    else {
      if(chave == vetor[tam-1]) return tam-1;
      else return  buscaSequencialRecursiva(vetor, tam-1, chave);
    }
}

