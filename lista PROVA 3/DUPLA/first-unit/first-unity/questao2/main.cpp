/**
 * A questão 2 tem como o objetivo implementar uma fila, usando duas pilhas. Para realizar 
 * uma adição, é necessário primeiramente fazer uma vefificação, esta é, se a pilha1 estiver vazia
 * será feita uma adição direta, para um segundo elemento adicionado, seria necessário ver se a pilha1 
 * está com algum elemento, caso exista será preciso retirar todos os elementos da pilha1 e colocar na pilha2,
 * para só assim fazer a adição do elemento desejado. Porém antes de terminar a adição do segundo elemento,
 * se faz necessário desempilhar todos os outros elementos da pilha2 e colocar na pilha1, e assim teremos 
 * o último elemento adicionado como primeiro. E assim para n elementos.
  */

#include "Fila.hpp"
int main()
{
    Fila fila(5);

    fila.enfileirar(1);
    fila.enfileirar(2);
    fila.enfileirar(3);
    fila.enfileirar(4);
    fila.enfileirar(5);

    fila.desenfileirar();
    fila.primentoElemento();
    fila.~Fila();
    return 0;
}