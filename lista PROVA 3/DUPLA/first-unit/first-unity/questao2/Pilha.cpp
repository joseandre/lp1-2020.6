#include "Pilha.hpp"
#include <iostream>

using namespace std;

Pilha::Pilha(int capacidade)
{
    this->capacidade = capacidade;
    topo = -1;
    elementos = new int[capacidade];
}

Pilha::~Pilha()
{
    delete[] elementos;
}

void Pilha::empilhar(int valor)
{
    if (pilhaCheia())
        cout << "Pilha cheia" << endl;

    ++topo;
    elementos[topo] = valor;
}

int Pilha::desempilhar()
{
    return elementos[topo--];
}

int Pilha::tamanhoPilha()
{
    return (topo + 1);
}

bool Pilha::pilhaCheia()
{
    return (tamanhoPilha() == capacidade) ? true : false;
}

int Pilha::elementoTopo()
{
    return (capacidade == 0) ? -1000000 : elementos[topo + 1];
}

int Pilha::elementoIndex(int index)
{
    return elementos[index];
}