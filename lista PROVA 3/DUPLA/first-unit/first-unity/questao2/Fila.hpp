#ifndef FILA_HPP
#define FILA_HPP

#include "Pilha.hpp"
#include <iostream>

using namespace std;

class Fila
{
private:
    Pilha *pilha1, *pilha2;
    int capacidade;

public:
    Fila(int);
    ~Fila();
    void enfileirar(int);
    int desenfileirar();
    int primeiro();
    void primentoElemento();
};

#endif