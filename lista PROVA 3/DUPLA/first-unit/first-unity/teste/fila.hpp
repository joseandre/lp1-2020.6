#ifndef FILA_HPP
#define FILA_HPP

class Fila
{
private:
    int *vetor;
    int capacidade;
    int contador;
    int primeiro;
    int ultimo;

public:
    Fila(int);
    ~Fila();
    void adicionar(int);
    int retirar();
    int tamanho();
    bool cheia();
    bool vazia();
    int topo();
    int fundo();
    
};

#endif