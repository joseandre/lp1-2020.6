#include "Fila.hpp"
#include "Pilha.hpp"
#include <iostream>

using namespace std;

Pilha::Pilha(int capacidade)
{
    fila1 = new Fila(capacidade);
    fila2 = new Fila(capacidade);
}

Pilha::~Pilha()
{
    delete fila1;
    delete fila2;
}

void Pilha::adicionarPilha(int valor)
{
    if (fila1->vazia())
        fila1->adicionar(valor);
    else
    {
        int tamanho = fila1->tamanho();
        for (int i = 0; i < tamanho; i++)
            fila2->adicionar(fila1->retirar());
        fila1->adicionar(valor);
        for (int i = 0; i < tamanho; i++)
            fila1->adicionar(fila2->retirar());
    }
}

int Pilha::retirarPilha()
{
    int elemento = fila1->retirar();
    return elemento;
}

int Pilha::tamanhoPilha()
{
    return ((fila1->vazia()) ? fila2->tamanho() : fila1->fundo());
}