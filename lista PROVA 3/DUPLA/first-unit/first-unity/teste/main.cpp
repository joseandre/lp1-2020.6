#include "fila.hpp"

#include <iostream>

using namespace std;

int main()
{
    Fila fila(5);

    fila.adicionar(1);
    fila.adicionar(2);
    fila.adicionar(3);
    fila.adicionar(4);
    fila.adicionar(5);
    fila.adicionar(6);

    cout << "Elemento topo: " << fila.topo() << endl;
    cout << "Elemento fundo: " << fila.fundo() << endl;

    fila.~Fila();
}