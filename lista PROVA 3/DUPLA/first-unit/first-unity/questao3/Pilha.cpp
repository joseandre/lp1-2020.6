#include "Pilha.hpp"

Pilha::Pilha(int capacidade)
{
    this->capacidade = capacidade;
    topo = -1;
    elementos = new int[capacidade];
}

Pilha::~Pilha()
{
    delete[] elementos;
}

void Pilha::empilhar(int valor)
{
    elementos[++topo] = valor;
}

int Pilha::desempilhar()
{
    return elementos[topo--];
}

int Pilha::pilhaTopo()
{
    return elementos[topo];
}

int Pilha::tamanhoPilha()
{
    return (topo + 1);
}

int Pilha::pilhaVazia()
{
    return topo == -1;
}

int Pilha::elementoIndex(int index)
{
    return elementos[index];
}