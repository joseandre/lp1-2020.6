
#include "Pilha.hpp"
#include <string.h>
#include <iostream>

using namespace std;

Pilha::Pilha(int tamanho)
{
    n = 0;
    capacidade = tamanho;
    elementos = new char[capacidade];
}

Pilha::~Pilha()
{
    delete[] elementos;
}

void Pilha::adicionar(int x)
{
    if (n < capacidade)
    {
        elementos[n] = x;
        n++;
    }
}

int Pilha::remover()
{
    if (n > 0)
    {
        int elemento = elementos[n - 1];
        n--;
        return elemento;
    }
    return -1;
}

int Pilha::tamanhoPilha()
{
    return n;
}
