/**
 * A lógica dessa questão em um primeiro momento foi verificar a quantidade de chaves que sem o sentido fechado e aberto.
 * Através da função númeroChaves podemos saber se a palavra que temos, possui o mesmo número de sinais abertos e fechados,
 * caso não possua, ou seja, temos números diferentes, então sabemos logo de cara que é uma expressão inválida.
 * Para a verificação é usado os métodos da pilha, e algumas comparações, por exemplo: palavra = {{}}
 * O algoritmo fará a verificação se o primeiro caracter é aberto, caso ele seja, será adicionado o valor 1 na pilha, e assim
 * para o segundo caracter que também é aberto. Até aí nossa pilha tem tamanho igual a 2.
 * O que nos resta agora é apenas "}}", para isso quando for feita a verificação novamente, cairá na condição de remover.
 * Quando chegar ao fim, teremos que nossa pilha tem tamanho 0 (pois foram feitas duas remoções). E assim se o tamanho for igual
 * a zero, temos que ela é uma palavra válida.
 */

#include "Pilha.hpp"
#include <iostream>
#include <string.h>

using namespace std;

void verificar(char *);

int main()
{
    char vetor[] = "aa{a\0";
    verificar(vetor);
}

bool numeroChaves(char *palavra, char aberto[], char fechado[])
{
    int cont1 = 0, cont2 = 0;
    for (int i = 0; i < strlen(palavra); i++)
    {
        if (palavra[i] == aberto[0])
            cont1++;
        if (palavra[i] == fechado[0])
            cont2++;
    }
    return (cont1 == cont2) ? true : false;
}

void verificar(char *palavra)
{
    Pilha pilha(strlen(palavra));
    char aberto[] = "{";
    char fechado[] = "}";

    if (numeroChaves(palavra, aberto, fechado) == true)
    {

        for (int i = 0; i < strlen(palavra); i++)
        {

            if (palavra[i] == aberto[0]) // aberto[0]
                pilha.adicionar(1);
            if (palavra[i] == fechado[0]) // fechado[0]
                pilha.remover();
        }
        cout << "Palavra válida!" << endl;
    }
    else
        cout << "Palavra não é válida!" << endl;
}