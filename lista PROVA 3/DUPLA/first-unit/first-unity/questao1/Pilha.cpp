#include "Fila.hpp"
#include "Pilha.hpp"
#include <iostream>

using namespace std;

Pilha::Pilha(int capacidade)
{
    fila1 = new Fila(capacidade);
    fila2 = new Fila(capacidade);
}

Pilha::~Pilha()
{
    delete fila1;
    delete fila2;
}

void Pilha::adicionarPilha(int valor)
{
    if (fila1->tamanho() == 0)
        fila1->enfileirar(valor);
    else
    {
        int tamanho = fila1->tamanho();
        for (int i = 0; i < tamanho; i++)
            fila2->enfileirar(fila1->remover());
        fila1->enfileirar(valor);
        for (int i = 0; i < tamanho; i++)
            fila1->enfileirar(fila2->remover());
    }
}

int Pilha::retirarPilha()
{
    int elemento = fila1->remover();
    cout << "Elemetento retirado: " << elemento << endl;
    return elemento;
}

int Pilha::tamanhoPilha()
{
    return fila1->tamanho() ? fila2->tamanho() : 1;
}

int Pilha::Elemento()
{
    return fila1->tamanho() > 0 ? fila1->elementoTopo() : -1;
}

void Pilha::ElementoTopo()
{
    if (Elemento() != -1)
        cout << "Elemento do topo: " << Elemento() << endl;
    else
        cout << "Pilha Vazia!" << endl;
}
