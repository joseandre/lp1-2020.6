
#include "Fila.hpp"
#include <iostream>

using namespace std;

Fila::Fila(int capacidade)
{
    n = 0;
    this->capacidade = capacidade;
    elementos = new int[capacidade];
    topo = 0;
    fim = 0;
}

Fila::~Fila()
{
    delete[] elementos;
}

void Fila::enfileirar(int x)
{
    if (n < capacidade)
    {
        elementos[fim] = x;
        fim = (fim + 1) % capacidade;
        n++;
    }
}

int Fila::remover()
{
    if (n > 0)
    {
        int value = elementos[topo];
        topo = (topo + 1) % capacidade;
        n--;
        return value;
    }
    return -1000000;
}

int Fila::elementoTopo()
{
    if (n > 0)
    {
        int value = elementos[topo];

        return value;
    }
    return -1000000; // valor de erro
}

int Fila::tamanho()
{
    return n;
}