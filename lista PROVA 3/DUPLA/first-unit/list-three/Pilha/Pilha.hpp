#ifndef PILHA_HPP
#define PILHA_HPP

class Pilha
{
private:
    int capacidade;
    int *elementos;
    int topo;
    int ultimo;

public:
    Pilha(int capacidade);
    ~Pilha();
    void empilhar(int valor);
    int desempilhar();
    bool PilhaCheia();
    int tamanhoPilha();
    int elementoTopo();
    void mostrarPilha();
};

#endif