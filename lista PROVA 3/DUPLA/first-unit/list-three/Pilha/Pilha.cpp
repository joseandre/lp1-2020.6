#include "Pilha.hpp"
#include <iostream>

using namespace std;

Pilha::Pilha(int capacidade) {
    topo = -1;
    this->capacidade = capacidade;
    elementos = new int[capacidade];
}

Pilha::~Pilha() {
    delete[] elementos;
}

void Pilha::empilhar(int valor) {

    if (PilhaCheia() != true) {
        topo++;
        elementos[topo] = valor;
        cout << "Empilhando: " << elementos[topo] << endl;
    }
}

int Pilha::desempilhar() {
    int aux = elementos[topo];
    cout << "Desempelinhando: " << aux << endl;
    topo--;
    return aux;
}

int Pilha::elementoTopo() {
    return elementos[topo];
}

bool Pilha::PilhaCheia() {
    if (tamanhoPilha() == capacidade)
        return true;
    return false;
}

int Pilha::tamanhoPilha() {
    return (topo + 1);
}

void Pilha::mostrarPilha() {
    for (int i = 0; i < capacidade; i++)
        cout << elementos[i] << " ";
    cout << endl;
}