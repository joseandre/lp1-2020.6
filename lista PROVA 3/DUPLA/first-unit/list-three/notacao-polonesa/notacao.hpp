#ifndef NOTACAO_HPP
#define NOTACAO_HPP

#include <string>

class Notacao
{
private:
    int *elementos;
    int aux;
    int tamanho;

public:
    Notacao(int);
    ~Notacao();
    bool PilhaCheia();
    void adicionar(int);
    int retirar();
    int notacao(std::string);
};

#endif