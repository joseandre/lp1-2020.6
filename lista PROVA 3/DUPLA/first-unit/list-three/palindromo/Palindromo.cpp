#include "Palindromo.hpp"
#include <iostream>
#include <string.h>

using namespace std;

Palindromo::Palindromo(char vetor[]) {
    tamanho = strlen(vetor);
    letra = -1;
    palavra = new char[tamanho];
}

void Palindromo::adicionar(char caracter) {
    palavra[++letra] = caracter;
}

char Palindromo::retirar() {
    return palavra[letra--];
}

int Palindromo::verificar(char vetor[]) {
    int meio = tamanho / 2,
        i;

    for (i = 0; i < meio; i++)
        adicionar(vetor[i]);

    if (tamanho % 2 != 0)
        i++;

    /* tamanho % 2 != 0 ? i++ : ; */

    while (vetor[i] != '\0') {
        char caracter = retirar();
        if (caracter != vetor[i])
            return 0;
        i++;
    }

    return 1;
}

Palindromo::~Palindromo() {
    delete[] palavra;
}
