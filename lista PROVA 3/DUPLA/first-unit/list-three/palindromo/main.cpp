#include "Palindromo.hpp"
#include <iostream>
#include <string>

using namespace std;

int main()
{

    string palavraString;
    cout << "Digite uma palavra: ";
    getline(cin, palavraString);

    int tamanho = palavraString.length();

    char palavra[tamanho];

    for (int i = 0; i < tamanho; i++)
        palavra[i] = palavraString[i];

    Palindromo palindromo(palavra);

    cout << (palindromo.verificar(palavra) ? "É um palidromo" : "Não é um palidromo!") << endl;

    palindromo.~Palindromo();
}
