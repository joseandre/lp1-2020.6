#ifndef LIST_H
#define LIST_H

typedef struct List
{
    int capacidade;
    int tamanho;
    int *elementos;
} list;

list *criar(int capacidade);
void destruir(list *l);
int adicionar(list *l, int valor, int posicao);

#endif LIST_H