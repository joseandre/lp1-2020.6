#include <iostream>

using namespace std;

void quickSort(int*, int, int);
int particionar(int*, int, int);

int main() {
  int n;
  
  cout << "Quantos elementos deseja adicionar: ";
  cin >> n;

  int s[n];

  for(int i = 0; i < n ; i++) 
    cin >> s[i];
    
  quickSort(s, 0, n-1);

  for(int i = 0; i < n ; i++) {
    cout << s[i] << " ";
  }

  cout << endl;
}


int particionar(int vetor[], int inicio, int fim) {
  int pivo = vetor[fim],
      index = inicio,
      i, aux;

  for(i = inicio; i < fim; i++) {
    if(vetor[i] <= pivo) {
      aux = vetor[i];
      vetor[i] = vetor[index];
      vetor[index] = aux;
      index++;
    }
  }
  aux = vetor[fim];
  vetor[fim] = vetor[index];
  vetor[index] = aux;

  return index;
}


void quickSort(int vetor[], int inicio, int fim) {
  if(inicio < fim ) {
    int index = particionar(vetor, inicio, fim);
    quickSort(vetor, inicio, index-1);
    quickSort(vetor, index +1 , fim);
  }
}
