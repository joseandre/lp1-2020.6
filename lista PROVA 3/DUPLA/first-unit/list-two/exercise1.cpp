#include <iostream>

using namespace std;

void selectionSort(int, int );

int main() {
  int n;
  
  cout << "Digite o tamanho do vetor: ";
  cin >> n;

  int s[n];

  for(int i = 0; i < n; i++) {
    cin >> s[i];
  }
  
  selectionSort(s, n);
}

void selectionSort(int vetor[], int n) {
 int aux, elem, meioIndex;
  
 for(int i = 0; i < n - 1 ; i++) {
   meioIndex = i ;

   for(int j = i +1; j < n; j++) {
     if(vetor[j] < vetor[meioIndex]) {
      meioIndex = j;
     }

     aux = vetor[meioIndex];
     vetor[meioIndex] = vetor[i];
     vetor[i] = aux;
   }
  
 }

 for(int i = 0; i < n; i++) 
   cout<< vetor[i] << " ";


}
