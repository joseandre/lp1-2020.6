#include "HashTable.hpp"
Hashtable::Hashtable() {
  for (int i = 0; i < tableSize; i++) {
    Hashing[i] = new item;
    Hashing[i]->value = -1;
    Hashing[i]->world = "empty";
    Hashing[i]->next = NULL;
  }
}

int Hashtable::hash(int key) {
  int hash = 0;
  int index;

  for (int i = 0; i < key; i++) {
    hash = (hash + key) * 13;
  }

  index = hash % tableSize;
  return index;
}

void Hashtable::Add(int valor, string _world) {
  int index = hash(valor);

  if (Hashing[index]->value == -1) {
    Hashing[index]->value = valor;
    Hashing[index]->world = _world;
  } else {
    item* Ptr = Hashing[index];
    item* n = new item;
    n->value = valor;
    n->world = _world;
    n->next = NULL;
    while (Ptr->next != NULL) {
      Ptr = Ptr->next;
    }
    Ptr->next = n;
  }
}

string Hashtable::FindWorld(int value) {
  int index = hash(value);
  item* Ptr = Hashing[index];

  if (Ptr->value == -1) {
    return "";
  } else {
    while (Ptr != NULL) {
      if (Ptr->value == value) {
        return Ptr->world;
      }
      Ptr = Ptr->next;
    }
    string result = Ptr->world;
    return result;
  }
}