#include "HashTable.hpp"

int main() {
  Hashtable hash;

  hash.Add(1, "Eu");
  hash.Add(2, "Tu");
  hash.Add(3, "Ele");
  hash.Add(4, "Nós");
  hash.Add(5, "Vós");
  hash.Add(6, "Eles");
  hash.Add(7, "Elas");

  //  cout << hash.FindWorld(20) << endl;
  cout << hash.FindWorld(3) << endl;

  return 0;
}