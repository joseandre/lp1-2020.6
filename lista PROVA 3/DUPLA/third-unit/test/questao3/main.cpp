/*
A lógica dessa questão é usar os algoritmos anteriores da lista 8 e implementar
apenas os métodos de união e interseção. Para a resolução do método de união, é
criado um novo conjunto C, realizar a soma dos elementos do conjunto A e B na
qual teremos o tamanho de cada conjunto e assim preenche-se o conjunto C com os
elementos do conjunto A e B. Para a resolução do método de interseção fiz
praticamente igual ao algoritmo passado, a única diferença é que peguei apenas
os elementos que são iguais em ambos vetores com o uso de dois laços de
repetição.
*/

#include "Set.hpp"

int main() {
  Set setA(5);
  Set setB(5);
  Set* setC;
  Set* setD;

  setA.insert(5);
  setA.insert(10);
  setA.insert(15);
  setA.insert(20);
  setA.insert(25);

  setB.insert(5);
  setB.insert(9);
  setB.insert(16);
  setB.insert(20);
  setB.insert(24);

  setA.print();
  setB.print();

  setC = setA.Union(&setA, &setB);
  cout << "Tamanho: " << setC->getCapacity() << endl;
  cout << "Conjunto C: ";
  setC->print();
  cout << endl;

  setD = setA.Inter(&setA, &setB);
  setD->print();

  cout << (setA.get(5) ? "Elemento encontrado" : "Elemento não econtrado!")
       << endl;
  cout << (setA.get(4) ? "Elemento encontrado" : "Elemento não econtrado!")
       << endl;

  cout << (setB.get(5) ? "Elemento encontrado" : "Elemento não econtrado!")
       << endl;
  cout << (setB.get(24) ? "Elemento encontrado" : "Elemento não econtrado!")
       << endl;

  return 0;
}