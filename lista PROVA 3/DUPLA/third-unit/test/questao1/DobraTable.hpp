#ifndef DOBRA_TABLE_HPP
#define DOBRA_TABLE_HPP

#include <iostream>
#include <math.h>

using namespace std;

const int CAPACITY_DOBRA = 11;

class DobraTable {
 private:
  int* table;
  int elements;
  int capacity;
  int dobra(int);

 public:
  DobraTable();
  DobraTable(int);

  ~DobraTable();

  void addDobra(int);
  bool get(int);
  int getCapacity();
  void print();
};

#endif