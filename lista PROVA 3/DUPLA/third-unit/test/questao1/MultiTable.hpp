#ifndef MULTI_TABLE_HPP
#define MULTI_TABLE_HPP

#include <iostream>
using namespace std;

const int CAPACITY_MULTI = 11;
const int STEPSIZE_MULTI = 3;

class MultiTable {
 private:
  int* table;
  int elements;
  int capacity;

 public:
  MultiTable();
  MultiTable(int);

  ~MultiTable();

  /* MULTIPLICATION */
  int hashMulti(int, int);
  void addMulti(int);

  int getCapacity();
  void print();
};

#endif