#include "MultiTable.hpp"

MultiTable::MultiTable() {
  elements = 0;
  capacity = CAPACITY_MULTI;

  table = new int[capacity];

  for (int i = 0; i < capacity; i++) {
    table[i] = -1;
  }
}

MultiTable::MultiTable(int value) {
  elements = 0;
  capacity = value;

  table = new int[capacity];

  for (int i = 0; i < capacity; i++) table[i] = -1;
}

int MultiTable::getCapacity() { 
  return capacity; 
}

MultiTable::~MultiTable() {
  delete[] table;
  table = NULL;
}

void MultiTable::print() {
    cout << endl << endl << "PRINTANDO TABELA: *** MULTIPLICACION *** " << endl << endl;

  for (int i = 0; i < capacity; i++) {
    if (table[i] != -1) cout << table[i] << " ";
  }
  cout << endl;
}

int MultiTable::hashMulti(int key, int size) {
  //  const int w = 32;
  const long long w_bit = 4294967295;
  const int A = 2654435769;
  long long r0 = key * A;
  return ((r0 & w_bit) >> (32 - size));
}

void MultiTable::addMulti(int key) {}