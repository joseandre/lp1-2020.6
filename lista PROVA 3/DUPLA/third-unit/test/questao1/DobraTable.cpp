#include "DobraTable.hpp"

DobraTable::DobraTable()
{
    elements = 0;
    capacity = CAPACITY_DOBRA;

    table = new int[capacity];

    for (int i = 0; i < capacity; i++)
    {
        table[i] = -1;
    }
}

DobraTable::DobraTable(int value)
{
    elements = 0;
    int aux = 1;

    while (pow(2, aux) < value)
    {
        aux++;
    }
    capacity = pow(2, aux);

    table = new int[capacity];

    for (int i = 0; i < capacity; i++)
        table[i] = -1;
}

int DobraTable::dobra(int num)
{
    int posicao;

    posicao = (((num & ((capacity - 1) << 7)) >> 7) ^ (num & (capacity - 1)));
    //posicao = (((928 & (127 << 7)) >> 7) ^ (928 & 127));

    return posicao;
}

void DobraTable::addDobra(int num)
{
    int key = dobra(num);
    int start = key;
    bool inserted = false;

    do
    {
        if (table[key] == -1)
        {
            table[key] = num;
            inserted = true;
        }
        else
        {
            key++;
            if (key == capacity)
                key = 0;
        }
    } while ((key != start) && (!inserted));

    if (!inserted)
        cerr << "Tabela cheia!" << endl;
}

int DobraTable::getCapacity()
{
    return capacity;
}

DobraTable::~DobraTable()
{
    delete[] table;
    table = NULL;
}

void DobraTable::print()
{
    cout << endl << endl << "PRINTANDO TABELA: *** DOBRA *** " << endl << endl;
    for (int i = 0; i < capacity; i++)
    {
        if (table[i] != -1)
            cout << table[i] << " ";
    }
    cout << endl
         << endl;
}

bool DobraTable::get(int num) {
  bool found = false;
  bool endSearch = false;
  int key = dobra(num);
  int startInd = key;

  do {
    if (table[key] == num) {
      endSearch = true;
      found = true;
    } else if (table[key] == -1)
      endSearch = true;
    else {
      key++;
      if (key == capacity) key = 0;
    }
  } while ((!endSearch) && (key != startInd));

  return found;
}
