#ifndef HASH_TABLE_HPP
#define HASH_TABLE_HPP

#include <iostream>
using namespace std;

const int CAPACITY_HASH = 11;
const int STEPSIZE_HASH = 3;

class HashTable {
 private:
  int* table;
  int elements;
  int capacity;

 public:
  HashTable();
  HashTable(int);

  ~HashTable();
  /* REST */
  void addRest(int);
  int hashValue(int, int);

  /* MULTIPLICATION */
  int hashMulti(int, int);
  void addMulti(int);

  bool get(int);
  int getCapacity();
  void print();
};

#endif