/*
  *   Para implementação dessa questão foram feitas três classes, para que tornasse o código mais organizado o que vai mudar são os 
   métodos para gerar a chave.
  *   Para a classe Rest, é usado o método hashvalue (método já usado em exercícios  anteriores), nele temos como a chave sendo o resto 
    da divisão entre o número que foi passado por parâmetro e a capacidade da lista.
  *   Para a dobra utulizamos um método que "divide" o número ao meio e vai utilizando o operador boleano XOR(OU exclusivo). Este método 
   vai dividindo ao meio e utilizando o operador boleano até que o número chege à um númeto em que a quantidade de bits se encaixe 
   com a capacidade de armazenamento da lista, assim a chave gerada não será um valor inexistente dentro da lista.
  *   Concluíndo o último método, o de Multiplicação, dado os valores da chave e tamanho, vou fazer as operações que são necessárias 
   de deslocamento e retornar uma posição.
*/

#include <cstdlib>
#include <ctime>
#include <iostream>

#include "HashTable.hpp"
#include "MultiTable.hpp"
#include "DobraTable.hpp"

using namespace std;

int main()
{
  HashTable resto(100);
  MultiTable multi(100);
  DobraTable dobra(100);

  int seed = 962;         // matrícula de José André
  srand(seed);

  int vetor[100];

  for (int i = 0; i < 100; i++)
  {
    vetor[i] = (rand() % 1000);

    resto.addRest(vetor[i]);
    multi.addMulti(vetor[i]);
    dobra.addDobra(vetor[i]);
  }

  resto.print();
  dobra.print();
  multi.print();
  
}