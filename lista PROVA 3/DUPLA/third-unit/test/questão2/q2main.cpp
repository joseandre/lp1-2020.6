/*
    *   Foi criado duas classes, uma é a classe List, que foi usada para se comportar como uma lista/vetor, e a outra foi a classe Tabela, 
    * que se comporta como a tabela, no caso, um ponteiro de ponteiro (mesma função que um vetor de vetor iria exercer).
    *   Fiz o algoritmo de uma forma que recebesse 10 números aleatórios, e um fator de carga de 0.75, como foi pedido no enunciado.
    *   Criei os métodos para gerar a chave de mapeamento dos elementos, criei um método para adicionar o elemento na tabela em sua devida
    * posição, criei um método para fazer a dobra do tamanho da tabela e realocar os elementos em seus novos lugares, já que como a 
    * tabela mudou de tamanho, sua função de mapeamento também mudou, e por fim, criei um método adicional para buscar a posição de cada
    * elemento dentro da tabela, este método imprime a chave e a posição do elemento dentro da chave. Fiz esse método para me dar a certeza que
    * eu estava alocando e realocando no lugar certo.
    *   Comentei nos arquivos .hpp as funções dos métodos e atributos para auxiliar no entendimento do código.
 */

#include <iostream>
#include <cstdlib>
#include "tabela.hpp"

using namespace std;

int main(){

    Tabela resto(10, 0.75);
    int seed = 962;
    srand(seed);

    int values [10];

    cout << endl;

    for(int i=0; i < 10; i++){
        values[i] = rand() % 1000;
        resto.set(values[i]);
    
    }

    cout << endl;

    resto.get(371);
    resto.get(411);
    resto.get(766);
    resto.get(771);

    cout << endl;

    return 0;
}