#include "tabela.hpp"
#include <iostream>

using namespace std;

Tabela::Tabela(int tam, float fCarga){
    tamanho = tam;
    fatorCarga = fCarga;
    quantidade = 0;
    tabela = new List *[tam];  

    for (int i = 0; i < tamanho; i++)
    {
        tabela[i] = new List;
        tabela[i]->value = i;       // a "cabeça" guarda a chave
    }
    
}

Tabela::~Tabela(){
for (int i = 0; i < tamanho; i++)
{
    delete [] tabela[i];
}


}

void Tabela::set(int num){
    if (quantidade >= (tamanho * fatorCarga))
    {
        cout << endl;
        fatorDeCarga();
        cout << endl;
    }
    
    int posicao = gerarChave(num);;
    int count = 0;

    List * elemento = new List;
    elemento->value = num;
    elemento->next = nullptr;

    List * current = tabela[posicao];

    while (current->next != nullptr)
    {
        current = current->next;
        count++;
    }

    current->next = elemento;
    cout << "Elemento " << num << " ALOCADO na chave: " << posicao << " na posição: " << count << endl << endl;

    quantidade++;
    
}

void Tabela::get(int num){
    int count = 0;
    int posicao = gerarChave(num);
	List * current = tabela[posicao]->next;

    if (tabela[posicao]->value == num)
    {
        cout << endl << "O número " << num << " está na chave: " << posicao << " na posição: " << count << endl;
        return;
    }else
    {
        while (current->value != -1)
        {
            if (current->value == num)
            {
                cout << endl << "O número " << num << " está na chave: " << posicao << " na posição: " << count << endl;
                break;
            }
            current = current->next;
            count++;
        }
        
    }

}

int Tabela::gerarChave(int num){
    return num % tamanho;
}

int Tabela::gerarChave2(int num){
    return num % (tamanho*2);
}

void Tabela::fatorDeCarga(){

    List ** tabela2 = new List *[tamanho*2];  

    for (int i = 0; i < tamanho*2; i++)
    {
        tabela2[i] = new List;
        tabela2[i]->value = i;           // a "cabeça" guarda a chave
    }

    for (int i = 0; i < tamanho; i++)
    {
	    List * aux = tabela[i]; 
        List * current = tabela[i]->next; 

        while (aux->next != nullptr )
        {
            
            if (tabela[i]->next != nullptr )
            {
                int posicao = gerarChave2(current->value);

                if (tabela2[posicao]->next == nullptr)
                {                
                    List * elemento = new List;
                    elemento->value = current->value;
                    elemento->next = nullptr;

                    tabela2[posicao]->next = elemento;

                    cout << "Elemento " << current->value << " REALOCADO na chave: " << posicao << " na posição: " << 0 << endl << endl;  

                    current = current->next;

                }else
                {
                    int count = 0;
                    List * current2 = tabela2[posicao];

                    List * elemento = new List;
                    elemento->value = current->value;
                    elemento->next = nullptr;

                    while (current2->next != nullptr)
                    {
                        current2 = current2->next;
                        count++;
                    }

                    current2->next = elemento;
                    cout << "Elemento " << current->value << " REALOXCADO na chave: " << posicao << " na posição: " << count << endl << endl; 

                    current = current->next; 
                }

            }
            aux = aux->next;
        }

    }
    tamanho = tamanho * 2;
    tabela = tabela2;
}