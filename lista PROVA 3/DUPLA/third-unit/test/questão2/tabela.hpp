#ifndef TABELA_HPP
#define TABELA_HPP

#include <iostream>
#include "list.hpp"

class Tabela {
	
	List ** tabela;							// tabela inicial
	int tamanho;							// atributo para armazenar a capacidade
	int quantidade;							// atributo para contar quantos elementos possui a tabela
	float fatorCarga;						//atributo que armazena o fator de carga

    int gerarChave(int num);				// método que gera a chave (posição)
    int gerarChave2(int num);				// método que gera a chave (posição) para a tabela dobrada

	public:	
		Tabela(int tam, float fatorCarga);	// construtor da classe
		~Tabela();							// destrutor da classe
		
		void set(int num);					// método que passa os elementos para a tabela dispersa 
		void get(int num);					// recupera um elemento na tabela dispersa
		void fatorDeCarga();				// método que faz a dobra da tabela e realoca os elementos
};

#endif