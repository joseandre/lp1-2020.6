#include "HashTable.hpp"

/* Nessa questão só hove a troca dos elementos */

int main()
{
    HashTable hash;

    hash.insert(1);
    hash.insert(2);
    hash.insert(3);
    hash.insert(4);
    hash.insert(5);
    hash.insert(6);
    hash.insert(7);
    hash.insert(8);
    hash.insert(9);
    hash.insert(10);

    cout << endl;

    cout << (hash.get(1) ? "Encontrado." : "Não Encontrado.") << endl;
    cout << (hash.get(5) ? "Encontrado." : "Não Encontrado.") << endl;
    cout << (hash.get(10) ? "Encontrado." : "Não Encontrado.") << endl;

    hash.insert(35);
    cout << endl;

    return 0;
}