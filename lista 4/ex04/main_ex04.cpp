// UTILIZANDO A ESTRUTURA DE PILHA PARA SABER SE A PALAVRA É UM POLÍNDROMO

#include "ex04.hpp"
#include <iostream>
#include "string.h"


using namespace std;

int main (){

	string palavra;

	cout << "Digite a palavra: ";
	getline(cin, palavra);

	Palin palin(palavra.length()); // construtor

	for (int i = 0; i < palavra.length(); i++){
			palin.set_elemento(palavra[i]); // adiciona elemento na pilha
	}

	cout << "O tamanho da palin é: " << palin.get_tamanho() << endl; // retorna o tamanho

	palin.get_elementos(); // retorna a palavra

	palin.comparar(); // compara se a palavra é um polindromo e mostra as comparações

	palin.remover(); // remove o elemento do topo

	palin.get_elementos(); // retorna a palavra

	palin.~Palin(); // destrutor

	return 0;
}