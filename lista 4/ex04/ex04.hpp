#ifndef EX04_HPP
#define EX04_HPP

class Palin {
	int capacidade;
	int tamanho;
	char * elementos;
	
	public:
	
	Palin(int capacidade);
	~Palin();
	void set_elemento(char letra);
	int get_tamanho();
	void get_elementos();
	int get_letra(char letra);
	int get_posicao(int pos);
	bool comparar_();
	void comparar();
	void remover();
};
#endif