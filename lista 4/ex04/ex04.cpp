#include "ex04.hpp"
#include <iostream>

using namespace std;

	Palin::Palin(int capacidade) {
		this->capacidade = capacidade;
		tamanho = 0;
		elementos = new char[capacidade];
	}

	Palin::~Palin() {
		delete [] elementos;
	}

	void Palin::set_elemento(char letra) {
		elementos[tamanho] = letra;
		tamanho++;
	}

	int Palin::get_tamanho(){
		return tamanho;
	}

	void Palin::get_elementos(){
		cout << "A palavra digitada foi: ";
		for (int i = 0; i < tamanho; i++) {
			cout << elementos[i];
		}
		cout << endl;
	}

	int Palin::get_letra(char letra){
		int index = -1;

		for (int i = 0; i < tamanho; i++) {
			if (letra == elementos[i]) {
				index = i;
				break;
			}
		}
		return index;
	}

	int Palin::get_posicao(int pos){
		return elementos[pos];
	}

	bool Palin::comparar_(){
		bool simNao = true;
		int i = 0, j = tamanho - 1;

		while (i < (tamanho/2)+1)
		{
			cout << "Letra da posicao " << i+1 << ": " << elementos[i] << "   --------   Letra da posicao " << j+1 << ": " << elementos[j] << endl;
			if (elementos[i] != elementos[j])
			{
				simNao = false;
			}
			
			i++;
			j--;
		}
		
		return simNao;
	}

	void Palin::comparar(){
		bool simNao = comparar_();
		if (simNao)
		{
			cout << "A palavra é um palíndromo! " << endl;
		}else{
			cout << "A palavra  NÃO é um palíndromo! " << endl;
		}
		
	}

	void Palin::remover(){
		cout << "Removendo a letra: " << elementos[0] << endl;
		
		elementos[tamanho-1] = 0;
		tamanho--;
	}