#include <iostream>
#include "lista.hpp"

using namespace std;

int main (){
	Lista lista(10);

	lista.set_elemento(1, 0);
	lista.set_elemento(2, 1);
	lista.set_elemento(3, 2);

	cout << "O valor está na posição: " << lista.get_valor(2) << endl;

	cout << "Na posição tem o valor: " << lista.get_posicao(2) << endl;	

	cout << "O tamanho da lista é: " << lista.get_tamanho_lista() << endl;

	lista.get_elementos();

	lista.remover(1);

	cout << "O tamanho da lista é: " << lista.get_tamanho_lista() << endl;

	lista.get_elementos();

	lista.~Lista();

	return 0;
}