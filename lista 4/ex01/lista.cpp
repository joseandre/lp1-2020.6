#include "lista.hpp"
#include <iostream>


using namespace std;

Lista::Lista(int capacidade){
	this->capacidade = capacidade;
	tamanho = 0;
	elementos = new int[capacidade];
}

Lista::~Lista(){
	delete [] elementos;
}

int Lista::set_elemento(int valor, int pos){
	if (tamanho < capacidade){	
		if (pos == tamanho){
			elementos[pos] = valor;
		} else {
			for (int i=tamanho;i>pos;i--){
				elementos [i] = elementos [i-1];
			}
			elementos [pos] = valor;
		}
		tamanho++;
		return 1;
	} else {
		return 0;
	}
}

int Lista::get_tamanho_lista(){
	return tamanho;
}

void Lista::get_elementos(){
	cout << "Os elementos são:";
    for (int i = 0; i < tamanho; i++) {
    	cout << " " << elementos[i];
    }
	cout << endl;
}


int Lista::get_valor(int valor){
	int index = -1;

    for (int i = 0; i < tamanho; i++) {
    	if (valor == elementos[i]) {
            index = i;
            break;
        }
    }
    return index;
}

void Lista::remover(int pos){
	for(int i = pos; i < tamanho - 1; i++) {
		elementos [i] = elementos [i + 1];
	}
	tamanho--;
}

int Lista::get_posicao(int pos){
	return elementos[pos];
}