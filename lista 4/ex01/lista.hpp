#ifndef LISTA_HPP_
#define LISTA_HPP_

class Lista {
	int capacidade;
	int tamanho;
	int * elementos;
	
	public:
	
	Lista(int capacidade);
	~Lista();
	int set_elemento(int valor, int pos);
	int get_tamanho_lista();
	void get_elementos();
	int get_valor(int valor);
	void remover(int pos);
	int get_posicao(int pos);
};

#endif
