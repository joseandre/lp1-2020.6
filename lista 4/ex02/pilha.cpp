#include "pilha.hpp"
#include <iostream>

using namespace std;

Pilha::Pilha(int capacidade) {
	this->capacidade = capacidade;
	tamanho = 0;
	elementos = new int[capacidade];
}

Pilha::~Pilha() {
	delete [] elementos;
}

void Pilha::set_elemento(int valor) {
	if (tamanho <= capacidade)
	{
		elementos[tamanho] = valor;
	tamanho++;
	}else{
		cout << "A pilha está cheia!!" << endl;
	}
	
}

int Pilha::get_tamanho(){
	if (tamanho > 0)
	{
	return tamanho;
	}else{
		return -1;
	}
	
}

int Pilha::get_Topo(){
	if (tamanho > 0)
	{
	return elementos[tamanho-1];
	}else{
		return -1;
	}
}

void Pilha::get_elementos(){
	cout << "Os elementos são:";
    for (int i = 0; i < tamanho; i++) {
    	cout << " " << elementos[i];
    }
	cout << endl;
}

int Pilha::get_valor(int valor){
	int aux = -1;

    for (int i = 0; i < tamanho; i++) {
    	if (valor == elementos[i]) {
            aux = i;
            break;
        }
    }
    return aux;
}

int Pilha::get_posicao(int pos){
	return elementos[pos];
}

void Pilha::remover(){
	if (tamanho > 0){
		elementos[tamanho-1] = 0;
		tamanho--;
	}else{
		cout << "Não existe elementos na lista!!" << endl;
	}
}
