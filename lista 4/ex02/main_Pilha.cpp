#include <iostream>
#include "pilha.hpp"

using namespace std;

int main (){
	Pilha pilha(10);

	pilha.set_elemento(1); //adiciona um elemento na lista
	pilha.set_elemento(2);
	pilha.set_elemento(3);

	cout << "O valor está na posição: " << pilha.get_valor(2) << endl; // busca o primeiro elemento igual o do parâmetro e diz qual é o índice dele

	cout << "Na posição tem o valor: " << pilha.get_posicao(2) << endl;	// busca o índice e diz qual elemento tem dentro dele

	cout << "O tamanho da pilha é: " << pilha.get_tamanho() << endl; //retorna o tamanho da pilha

	pilha.get_elementos(); //imprime os elementos

	pilha.remover(); // remove o elemento que está no topo da lista

	cout << "O tamanho da pilha é: " << pilha.get_tamanho() << endl; //retorna o tamanho da pilha
    
	cout << "O topo da pilha é: " << pilha.get_Topo() << endl; //retorna o topo da lista

    pilha.get_elementos(); //imprime os elementos

	pilha.~Pilha(); // destrutor da classe

	return 0;
}