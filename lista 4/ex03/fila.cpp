#include "Fila.hpp"
#include <iostream>

using namespace std;

Fila::Fila(int capacidade) {
	this->capacidade = capacidade;
	fim = 0;
	ini = 0;
	tamanho = 0;
	elementos = new int[capacidade];
}

Fila::~Fila() {
	delete [] elementos;
}

void Fila::set_elemento(int valor) {
	if (fim == ini)
	{
		fim = 0;
		ini = 0;
	}
	
	if (fim <= capacidade)
	{
		elementos[fim] = valor;
	fim++;
	tamanho++;
	}else{
		cout << "A pilha está cheia!!" << endl;
	}
}

int Fila::get_tamanho(){
	return tamanho;
}

void Fila::get_elementos(){
	cout << "Os elementos são:";
    for (int i = ini; i < fim; i++) {
    	cout << " " << elementos[i];
    }
	cout << endl;
}

int Fila::get_valor(int valor){
	int index = -1;

    for (int i = 0; i < fim; i++) {
    	if (valor == elementos[i]) {
            index = i;
            break;
        }
    }
    return index;
}

/*int Fila::get_posicao(int pos){
	return elementos[pos];
}*/

void Fila::remover(){
	if (fim != ini){
		elementos[ini] = -1;
		ini++;
		tamanho--;
	}else{
		cout << "Não existe elementos na lista!!" << endl;
	}
}
