#ifndef FILA_HPP
#define FILA_HPP

class Fila {
	int capacidade;
	int ini, fim, tamanho;
	int * elementos;
	
	public:
	
	Fila(int capacidade);
	~Fila();
	void set_elemento(int valor);
	int get_tamanho();
	void get_elementos();
	int get_valor(int valor);
	//int get_posicao(int pos);
	void remover();
};
#endif