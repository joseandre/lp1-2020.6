/*
	* O algoritmo faz uso da estrutura pilha e de lista encadeada. 
	* O algoritmo é uma pilha usando a lista encadeada, ele possui os métodos de uma pilha, mas foram implementados por meio de uma
	  lista encadeada.
	* Para guardar o elemento do topo eu utilizei o tail da lista encadeada, pois ele é o elemento do fim, que no caso também é o último 
	  elemento que foi adicionado, ou seja, o topo.
	* Comentei o que cada método faz nos arquivos .hpp e aqui na main.
	* Aqui na main possui 5 exemplos para testar o algoritmo, apenas o exemplo 1 está descomentado, caso queira executar os outros, basta 
	  descomentar eles. Tentei mesclar ao máximo a chamada dos métodos e sempre suprir o que era pedido na questão.
	* Professor, o senhor disse que usando o head e o tail o programa não seria tão eficiente, mas eu acho que seria, pois a busca pelo fim da fila(tail), fica com uma complexidade de O(1), porque a busca é feita de forma direta, diferente de quando eu uso apenas o inicio(head) que fica com a complexidade de O(n). Como foi pedido uma lista encadeada simples, não utilizei o antecessor (previus). 
*/

#include <iostream>
#include "pilha.hpp"

using namespace std;

int main (){
	Pilha pilha1;
	Pilha pilha2;
	Pilha pilha3;
	Pilha pilha4;
	Pilha pilha5;

    ///****** EXEMPLO 1 ******///

	pilha1.set_elemento(1); //adiciona um elemento na lista
	pilha1.set_elemento(2);
	pilha1.set_elemento(3);

	cout << endl << "Removendo o elemento: " << pilha1.remover() << endl; // remove o elemento que está no topo da pilha1
	cout << "Removendo o elemento: " << pilha1.remover() << endl;
	cout << "Removendo o elemento: " << pilha1.remover() << endl;

	pilha1.set_elemento(2);
	pilha1.set_elemento(3);

	cout << endl << "O topo da pilha1 é: " << pilha1.get_Topo() << endl; //retorna o elemento do topo da pilha1

	cout << "O tamanho da pilha1 é: " << pilha1.get_tamanho() << endl; //retorna o tamanho da pilha1

	cout << endl << "Removendo o elemento: " << pilha1.remover() << endl;

	pilha1.set_elemento(2);
	pilha1.set_elemento(3);
	pilha1.set_elemento(4);
	pilha1.set_elemento(6);
	pilha1.set_elemento(10);

	cout << endl << "O topo da pilha1 é: " << pilha1.get_Topo() << endl; 

	cout << "O tamanho da pilha1 é: " << pilha1.get_tamanho() << endl << endl;

	pilha1.~Pilha(); // destrutor da classe
	


	///****** EXEMPLO 2 ******///

	/*pilha2.set_elemento(1); //adiciona um elemento na lista
	pilha2.set_elemento(2);
	pilha2.set_elemento(3);
	pilha2.set_elemento(2);
	pilha2.set_elemento(3);

	cout << endl << "Removendo o elemento: " << pilha2.remover() << endl; // remove o elemento que está no topo da pilha2
	cout << "Removendo o elemento: " << pilha2.remover() << endl;
	cout << "Removendo o elemento: " << pilha2.remover() << endl;

	cout << endl << "O topo da pilha2 é: " << pilha2.get_Topo() << endl; //retorna o elemento do topo da pilha2

	cout << "O tamanho da pilha2 é: " << pilha2.get_tamanho() << endl; //retorna o tamanho da pilha2

	cout << endl << "Removendo o elemento: " << pilha2.remover() << endl;
	cout << "Removendo o elemento: " << pilha2.remover() << endl;

	cout << endl << "O tamanho da pilha2 é: " << pilha2.get_tamanho() << endl;

	pilha2.set_elemento(2);
	pilha2.set_elemento(3);
	pilha2.set_elemento(4);
	pilha2.set_elemento(6);
	pilha2.set_elemento(10);

	cout << endl << "O topo da pilha2 é: " << pilha2.get_Topo() << endl; 

	cout << "O tamanho da pilha2 é: " << pilha2.get_tamanho() << endl << endl;

	pilha2.~Pilha(); // destrutor da classe
	*/


 	///****** EXEMPLO 3 ******///

	/*pilha3.set_elemento(1); //adiciona um elemento na lista
	pilha3.set_elemento(2);
	

	cout << endl << "Removendo o elemento: " << pilha3.remover() << endl; // remove o elemento que está no topo da pilha3
	cout << "Removendo o elemento: " << pilha3.remover() << endl << endl;

	pilha3.set_elemento(3);
	pilha3.set_elemento(2);
	pilha3.set_elemento(3);

	cout << "Removendo o elemento: " << pilha3.remover() << endl;

	cout << endl << "O topo da pilha3 é: " << pilha3.get_Topo() << endl; //retorna o elemento do topo da pilha3

	cout << "O tamanho da pilha3 é: " << pilha3.get_tamanho() << endl; //retorna o tamanho da pilha3

	cout << endl << "Removendo o elemento: " << pilha3.remover() << endl;
	cout << "Removendo o elemento: " << pilha3.remover() << endl;

	cout << endl << "O tamanho da pilha3 é: " << pilha3.get_tamanho() << endl;

	pilha3.set_elemento(2);
	pilha3.set_elemento(3);

	cout << endl << "Removendo o elemento: " << pilha3.remover() << endl;

	pilha3.set_elemento(4);
	pilha3.set_elemento(6);
	pilha3.set_elemento(10);

	cout << endl << "O topo da pilha3 é: " << pilha3.get_Topo() << endl; 

	cout << "O tamanho da pilha3 é: " << pilha3.get_tamanho() << endl << endl;

	pilha3.~Pilha(); // destrutor da classe
	*/


	///****** EXEMPLO 4 ******///

	/*pilha4.set_elemento(1); //adiciona um elemento na lista
	pilha4.set_elemento(2);
	pilha4.set_elemento(2);
	
	cout << endl << "Removendo o elemento: " << pilha4.remover() << endl; // remove o elemento que está no topo da pilha4
	cout << "Removendo o elemento: " << pilha4.remover() << endl << endl;

	pilha4.set_elemento(3);
	pilha4.set_elemento(3);

	cout << "Removendo o elemento: " << pilha4.remover() << endl;

	cout << endl << "O topo da pilha4 é: " << pilha4.get_Topo() << endl; //retorna o elemento do topo da pilha4

	cout << "O tamanho da pilha4 é: " << pilha4.get_tamanho() << endl; //retorna o tamanho da pilha4

	cout << endl << "Removendo o elemento: " << pilha4.remover() << endl;
	cout << "Removendo o elemento: " << pilha4.remover() << endl;

	cout << endl << "O tamanho da pilha4 é: " << pilha4.get_tamanho() << endl;

	pilha4.set_elemento(2);
	pilha4.set_elemento(3);
	pilha4.set_elemento(4);
	pilha4.set_elemento(6);

	cout << endl << "Removendo o elemento: " << pilha4.remover() << endl;
	cout << "Removendo o elemento: " << pilha4.remover() << endl;
	
	pilha4.set_elemento(10);

	cout << endl << "O topo da pilha4 é: " << pilha4.get_Topo() << endl; 

	cout << "O tamanho da pilha4 é: " << pilha4.get_tamanho() << endl << endl;

	pilha4.~Pilha(); // destrutor da classe
	*/


	///****** EXEMPLO 5 ******///

	/*pilha5.set_elemento(1); //adiciona um elemento na lista
	pilha5.set_elemento(9);
	pilha5.set_elemento(7);
	
	cout << endl << "Removendo o elemento: " << pilha5.remover() << endl; // remove o elemento que está no topo da pilha5
	cout << "Removendo o elemento: " << pilha5.remover() << endl << endl;

	pilha5.set_elemento(3);
	pilha5.set_elemento(9);

	cout << "Removendo o elemento: " << pilha5.remover() << endl;
	cout << "Removendo o elemento: " << pilha5.remover() << endl;

	cout << endl << "O topo da pilha5 é: " << pilha5.get_Topo() << endl; //retorna o elemento do topo da pilha5

	cout << "O tamanho da pilha5 é: " << pilha5.get_tamanho() << endl; //retorna o tamanho da pilha5

	cout << endl << "Removendo o elemento: " << pilha5.remover() << endl;

	cout << endl << "O tamanho da pilha5 é: " << pilha5.get_tamanho() << endl;

	pilha5.set_elemento(21);
	pilha5.set_elemento(13);
	pilha5.set_elemento(4);
	pilha5.set_elemento(26);

	cout << endl << "Removendo o elemento: " << pilha5.remover() << endl;
	cout << "Removendo o elemento: " << pilha5.remover() << endl;
	
	pilha5.set_elemento(10);

	cout << endl << "O topo da pilha5 é: " << pilha5.get_Topo() << endl; 

	cout << "O tamanho da pilha5 é: " << pilha5.get_tamanho() << endl << endl;

	cout << endl << "Removendo o elemento: " << pilha5.remover() << endl; // remove o elemento que está no topo da pilha5
	cout << "Removendo o elemento: " << pilha5.remover() << endl << endl;
	cout << "Removendo o elemento: " << pilha5.remover() << endl << endl;

	cout << "O tamanho da pilha5 é: " << pilha5.get_tamanho() << endl << endl;

	pilha5.~Pilha(); // destrutor da classe
	*/

	return 0;
}