
#ifndef NODE_HPP_
#define NODE_HPP_

class Node {
	public:
		int value;		//atributo que armazena o valor do nó
		Node * next;	//atributo que armazena o próximo nó
		
		Node(int v);	//construtor da classe
};


#endif
