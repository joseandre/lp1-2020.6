#include "pilha.hpp"
#include <iostream>

using namespace std;

Pilha::Pilha() {
	head = nullptr;
	tail = nullptr;
	tamanho = 0;

}

Pilha::~Pilha() {
	while(head != nullptr){
		Node * current = head;
		head = head->next;
		delete current;
	}
    delete head;
}

void Pilha::set_elemento(int valor) {
	Node* newNode = new Node(valor);

	if (head == nullptr)
	{
		head = newNode;
		tail = newNode;
		tamanho++;

	}else 
	{
		tail->next = newNode;
		tail = newNode;
		tamanho++;

	}

	return;
}

int Pilha::get_tamanho(){
	if (tamanho >= 0)
	{
	return tamanho;
	}else{
		return -1;
	}
	
}

int Pilha::get_Topo(){
	if (tamanho > 0)
	{
	return tail->value;
	}else{
		return -1;
	}
}

int Pilha::remover(){
	if(tamanho == 0){
		cout << "[ERRO!!!]   Não existe elementos na lista. " << endl;

		return -1;
	}

	int aux1 = tail->value;
    Node *  current = head;

	if (tamanho == 1)
	{
		delete tail;
		delete head;
		tamanho--;
	}else
	{
		while(current->next->next != nullptr){
		current = current->next;
		}

		delete tail;

		tail = current;
		tail->next = nullptr;

		tamanho--;
	}
	
	return aux1;
}
