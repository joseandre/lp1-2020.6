#ifndef PILHA_HPP
#define PILHA_HPP

#include "Node.hpp"

class Pilha {
	int tamanho;					//atributo que armazena o tamanho da pilha
	Node * head;					//atributo que armazena o elemento que foi adicionado por primeiro na pilha
	Node * tail;					//atributo que armazena o topo da pilha
	
	public:
	
	Pilha();						//construtor da classe
	~Pilha();						//destrutor da classe
	void set_elemento(int valor);	//método para adicionar elementos ao topo da pilha
	int get_Topo();					//método que retorna o elemento do topo da pilha
	int get_tamanho();				//método que retorna o tamanho da pilha
	int remover();					//método que remove o elemento do topo da pilha
};
#endif