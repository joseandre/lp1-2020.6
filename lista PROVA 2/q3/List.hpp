
#ifndef LIST_HPP_
#define LIST_HPP_

#include <iostream>
#include "Node.hpp"

using namespace std;

class List {
	Node * head;						//atributo que armazena qual é o nó inicial
	Node * getNode(int x);				//método que retorna um posição na lista que foi passado por parâmetro
	
	public:
		List();							//construtor da classe
		~List();						//destrutor da classe
		
		bool add(int x, int pos);		//método para adicionar um elemento na posição que foi passada por parâmetro
		int remove(int pos);			//método para remover o nó da posição que foi passada por parâmetro
		int get(int x);					//método para retornar a posição do elemento que foi passado por parâmetro
		int size();						//método que retorna o tamanho da lista
		void inverter();				//método para inverter a ordem da lista de tras para frente

};

#endif