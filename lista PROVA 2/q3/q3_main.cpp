/*
    * Eu criei um método chamado 'inverter', este método tem a função de inverter a ordem da lista de tras para frente.Ele inverte os valores que contem nos 
      atributos 'value', inverte o primeiro com o último, o segundo com o penultimo e assim em diante.
    * Neste método eu fiz um tratamento que verifica se a quantidade de elementos é par ou ímpar, pois se for ímpar, o elemento do meio não de altera.
    * Comentei o que cada método faz aqui na main e nos arquivos .hpp.
    * Aqui na main foram feitos 3 exemplos, o exemplo 1 adiciona um nó sempre ao fim da lista, o exemplo 2 adiciona sempro no início da lista, e o exemplo 3 
      adiciona sempre nas posições 0, 1, 2, 3 e 4. Apenas o exemplo 1 está descomentado, para executar os outros basta descomentar e comentar o que está descomentado, pois 
      eu só criei um objeto do tipo List creio que executar as três de uma vez também não ficaria tão trivial de se analisar no prompt de comandos, por isso não 
      instanciei 3 objetos diferentes.
*/

#include <iostream>
#include "List.hpp"

using namespace std;

int main (){
	List list;

    ///****** EXEMPLO 1 = ADICIONANDO SEMPRE NO FIM DA LISTA ******///

	list.add(1, 0); //adiciona um elemento na lista
	list.add(2, 1);
	list.add(3, 2);
    list.add(4, 3);
    list.add(5, 4);
    list.add(6, 5);
    list.add(7, 6);
    list.add(8, 7);
    list.add(9, 8);
    list.add(10, 9);


	cout << endl << "O numero 1 esta na posicao: " << list.get(1) << endl; // retorna a posição do elemento que foi passado por parâmetro
    cout << "O numero 2 esta na posicao: " << list.get(2) << endl;
    cout << "O numero 3 esta na posicao: " << list.get(3) << endl;
    cout << "O numero 4 esta na posicao: " << list.get(4) << endl;
    cout << "O numero 5 esta na posicao: " << list.get(5) << endl;
    cout << "O numero 6 esta na posicao: " << list.get(6) << endl;
    cout << "O numero 7 esta na posicao: " << list.get(7) << endl;
    cout << "O numero 8 esta na posicao: " << list.get(8) << endl;
    cout << "O numero 9 esta na posicao: " << list.get(9) << endl;
    cout << "O numero 10 esta na posicao: " << list.get(10) << endl;

    cout << endl << "O tamanho da lista e: " << list.size() << endl << endl;    //retorna o tamanho da lista

    list.inverter();                                                            //inverte de tras para frente a lista

    cout << endl << "O numero 1 esta na posicao: " << list.get(1) << endl; 
    cout << "O numero 2 esta na posicao: " << list.get(2) << endl;
    cout << "O numero 3 esta na posicao: " << list.get(3) << endl;
    cout << "O numero 4 esta na posicao: " << list.get(4) << endl;
    cout << "O numero 5 esta na posicao: " << list.get(5) << endl;
    cout << "O numero 6 esta na posicao: " << list.get(6) << endl;
    cout << "O numero 7 esta na posicao: " << list.get(7) << endl;
    cout << "O numero 8 esta na posicao: " << list.get(8) << endl;
    cout << "O numero 9 esta na posicao: " << list.get(9) << endl;
    cout << "O numero 10 esta na posicao: " << list.get(10) << endl;

    cout << endl << "O tamanho da lista e: " << list.size() << endl << endl;

    list.inverter();            

    cout << endl << "O numero 1 esta na posicao: " << list.get(1) << endl;  
    cout << "O numero 2 esta na posicao: " << list.get(2) << endl;
    cout << "O numero 3 esta na posicao: " << list.get(3) << endl;
    cout << "O numero 4 esta na posicao: " << list.get(4) << endl;
    cout << "O numero 5 esta na posicao: " << list.get(5) << endl;
    cout << "O numero 6 esta na posicao: " << list.get(6) << endl;
    cout << "O numero 7 esta na posicao: " << list.get(7) << endl;
    cout << "O numero 8 esta na posicao: " << list.get(8) << endl;
    cout << "O numero 9 esta na posicao: " << list.get(9) << endl;
    cout << "O numero 10 esta na posicao: " << list.get(10) << endl;

    cout << endl << "O tamanho da lista e: " << list.size() << endl << endl;

    list.add(11, 10);
    list.add(12, 11);
    list.add(13, 12);
    list.add(14, 13);
    list.add(15, 14);

    cout << endl << "O numero 1 esta na posicao: " << list.get(1) << endl;  
    cout << "O numero 2 esta na posicao: " << list.get(2) << endl;
    cout << "O numero 3 esta na posicao: " << list.get(3) << endl;
    cout << "O numero 4 esta na posicao: " << list.get(4) << endl;
    cout << "O numero 5 esta na posicao: " << list.get(5) << endl;
    cout << "O numero 6 esta na posicao: " << list.get(6) << endl;
    cout << "O numero 7 esta na posicao: " << list.get(7) << endl;
    cout << "O numero 8 esta na posicao: " << list.get(8) << endl;
    cout << "O numero 9 esta na posicao: " << list.get(9) << endl;
    cout << "O numero 10 esta na posicao: " << list.get(10) << endl;
    cout << "O numero 11 esta na posicao: " << list.get(11) << endl;
    cout << "O numero 12 esta na posicao: " << list.get(12) << endl;
    cout << "O numero 13 esta na posicao: " << list.get(13) << endl;
    cout << "O numero 14 esta na posicao: " << list.get(14) << endl;
    cout << "O numero 15 esta na posicao: " << list.get(15) << endl;

    cout << endl << "O tamanho da lista e: " << list.size() << endl << endl;

    list.inverter();

    cout << endl << "O numero 1 esta na posicao: " << list.get(1) << endl; 
    cout << "O numero 2 esta na posicao: " << list.get(2) << endl;
    cout << "O numero 3 esta na posicao: " << list.get(3) << endl;
    cout << "O numero 4 esta na posicao: " << list.get(4) << endl;
    cout << "O numero 5 esta na posicao: " << list.get(5) << endl;
    cout << "O numero 6 esta na posicao: " << list.get(6) << endl;
    cout << "O numero 7 esta na posicao: " << list.get(7) << endl;
    cout << "O numero 8 esta na posicao: " << list.get(8) << endl;
    cout << "O numero 9 esta na posicao: " << list.get(9) << endl;
    cout << "O numero 10 esta na posicao: " << list.get(10) << endl;
    cout << "O numero 11 esta na posicao: " << list.get(11) << endl;
    cout << "O numero 12 esta na posicao: " << list.get(12) << endl;
    cout << "O numero 13 esta na posicao: " << list.get(13) << endl;
    cout << "O numero 14 esta na posicao: " << list.get(14) << endl;
    cout << "O numero 15 esta na posicao: " << list.get(15) << endl;

    cout << endl << "O tamanho da lista e: " << list.size() << endl << endl;

    list.add(16, 15);
    list.add(17, 16);
    list.add(18, 17);
    list.add(19, 18);
    list.add(20, 19);

    cout << endl << "O numero 1 esta na posicao: " << list.get(1) << endl; 
    cout << "O numero 2 esta na posicao: " << list.get(2) << endl;
    cout << "O numero 3 esta na posicao: " << list.get(3) << endl;
    cout << "O numero 4 esta na posicao: " << list.get(4) << endl;
    cout << "O numero 5 esta na posicao: " << list.get(5) << endl;
    cout << "O numero 6 esta na posicao: " << list.get(6) << endl;
    cout << "O numero 7 esta na posicao: " << list.get(7) << endl;
    cout << "O numero 8 esta na posicao: " << list.get(8) << endl;
    cout << "O numero 9 esta na posicao: " << list.get(9) << endl;
    cout << "O numero 10 esta na posicao: " << list.get(10) << endl;
    cout << "O numero 11 esta na posicao: " << list.get(11) << endl;
    cout << "O numero 12 esta na posicao: " << list.get(12) << endl;
    cout << "O numero 13 esta na posicao: " << list.get(13) << endl;
    cout << "O numero 14 esta na posicao: " << list.get(14) << endl;
    cout << "O numero 15 esta na posicao: " << list.get(15) << endl;
    cout << "O numero 16 esta na posicao: " << list.get(16) << endl;
    cout << "O numero 17 esta na posicao: " << list.get(17) << endl;
    cout << "O numero 18 esta na posicao: " << list.get(18) << endl;
    cout << "O numero 19 esta na posicao: " << list.get(19) << endl;
    cout << "O numero 20 esta na posicao: " << list.get(20) << endl;

    cout << endl << "O tamanho da lista e: " << list.size() << endl << endl;

    list.inverter();

    cout << endl << "O numero 1 esta na posicao: " << list.get(1) << endl; 
    cout << "O numero 2 esta na posicao: " << list.get(2) << endl;
    cout << "O numero 3 esta na posicao: " << list.get(3) << endl;
    cout << "O numero 4 esta na posicao: " << list.get(4) << endl;
    cout << "O numero 5 esta na posicao: " << list.get(5) << endl;
    cout << "O numero 6 esta na posicao: " << list.get(6) << endl;
    cout << "O numero 7 esta na posicao: " << list.get(7) << endl;
    cout << "O numero 8 esta na posicao: " << list.get(8) << endl;
    cout << "O numero 9 esta na posicao: " << list.get(9) << endl;
    cout << "O numero 10 esta na posicao: " << list.get(10) << endl;
    cout << "O numero 11 esta na posicao: " << list.get(11) << endl;
    cout << "O numero 12 esta na posicao: " << list.get(12) << endl;
    cout << "O numero 13 esta na posicao: " << list.get(13) << endl;
    cout << "O numero 14 esta na posicao: " << list.get(14) << endl;
    cout << "O numero 15 esta na posicao: " << list.get(15) << endl;
    cout << "O numero 16 esta na posicao: " << list.get(16) << endl;
    cout << "O numero 17 esta na posicao: " << list.get(17) << endl;
    cout << "O numero 18 esta na posicao: " << list.get(18) << endl;
    cout << "O numero 19 esta na posicao: " << list.get(19) << endl;
    cout << "O numero 20 esta na posicao: " << list.get(20) << endl;

    cout << endl << "O tamanho da lista e: " << list.size() << endl << endl;


    ///****** EXEMPLO 2 = ADICIONANDO SEMPRE NO INICIO DA LISTA ******///

    /*list.add(1, 0); //adiciona um elemento na lista
	list.add(2, 0);
	list.add(3, 0);
    list.add(4, 0);
    list.add(5, 0);
    list.add(6, 0);
    list.add(7, 0);
    list.add(8, 0);
    list.add(9, 0);
    list.add(10, 0);


	cout << endl << "O numero 1 esta na posicao: " << list.get(1) << endl; // retorna a posição do elemento que foi passado por parâmetro
    cout << "O numero 2 esta na posicao: " << list.get(2) << endl;
    cout << "O numero 3 esta na posicao: " << list.get(3) << endl;
    cout << "O numero 4 esta na posicao: " << list.get(4) << endl;
    cout << "O numero 5 esta na posicao: " << list.get(5) << endl;
    cout << "O numero 6 esta na posicao: " << list.get(6) << endl;
    cout << "O numero 7 esta na posicao: " << list.get(7) << endl;
    cout << "O numero 8 esta na posicao: " << list.get(8) << endl;
    cout << "O numero 9 esta na posicao: " << list.get(9) << endl;
    cout << "O numero 10 esta na posicao: " << list.get(10) << endl;

    cout << endl << "O tamanho da lista e: " << list.size() << endl << endl;    //retorna o tamanho da lista

    list.inverter();                                                            //inverte de tras para frente a lista

    cout << endl << "O numero 1 esta na posicao: " << list.get(1) << endl; 
    cout << "O numero 2 esta na posicao: " << list.get(2) << endl;
    cout << "O numero 3 esta na posicao: " << list.get(3) << endl;
    cout << "O numero 4 esta na posicao: " << list.get(4) << endl;
    cout << "O numero 5 esta na posicao: " << list.get(5) << endl;
    cout << "O numero 6 esta na posicao: " << list.get(6) << endl;
    cout << "O numero 7 esta na posicao: " << list.get(7) << endl;
    cout << "O numero 8 esta na posicao: " << list.get(8) << endl;
    cout << "O numero 9 esta na posicao: " << list.get(9) << endl;
    cout << "O numero 10 esta na posicao: " << list.get(10) << endl;

    cout << endl << "O tamanho da lista e: " << list.size() << endl << endl;

    list.inverter();            

    cout << endl << "O numero 1 esta na posicao: " << list.get(1) << endl;  
    cout << "O numero 2 esta na posicao: " << list.get(2) << endl;
    cout << "O numero 3 esta na posicao: " << list.get(3) << endl;
    cout << "O numero 4 esta na posicao: " << list.get(4) << endl;
    cout << "O numero 5 esta na posicao: " << list.get(5) << endl;
    cout << "O numero 6 esta na posicao: " << list.get(6) << endl;
    cout << "O numero 7 esta na posicao: " << list.get(7) << endl;
    cout << "O numero 8 esta na posicao: " << list.get(8) << endl;
    cout << "O numero 9 esta na posicao: " << list.get(9) << endl;
    cout << "O numero 10 esta na posicao: " << list.get(10) << endl;

    cout << endl << "O tamanho da lista e: " << list.size() << endl << endl;

    list.add(11, 0);
    list.add(12, 0);
    list.add(13, 0);
    list.add(14, 0);
    list.add(15, 0);

    cout << endl << "O numero 1 esta na posicao: " << list.get(1) << endl;  
    cout << "O numero 2 esta na posicao: " << list.get(2) << endl;
    cout << "O numero 3 esta na posicao: " << list.get(3) << endl;
    cout << "O numero 4 esta na posicao: " << list.get(4) << endl;
    cout << "O numero 5 esta na posicao: " << list.get(5) << endl;
    cout << "O numero 6 esta na posicao: " << list.get(6) << endl;
    cout << "O numero 7 esta na posicao: " << list.get(7) << endl;
    cout << "O numero 8 esta na posicao: " << list.get(8) << endl;
    cout << "O numero 9 esta na posicao: " << list.get(9) << endl;
    cout << "O numero 10 esta na posicao: " << list.get(10) << endl;
    cout << "O numero 11 esta na posicao: " << list.get(11) << endl;
    cout << "O numero 12 esta na posicao: " << list.get(12) << endl;
    cout << "O numero 13 esta na posicao: " << list.get(13) << endl;
    cout << "O numero 14 esta na posicao: " << list.get(14) << endl;
    cout << "O numero 15 esta na posicao: " << list.get(15) << endl;

    cout << endl << "O tamanho da lista e: " << list.size() << endl << endl;

    list.inverter();

    cout << endl << "O numero 1 esta na posicao: " << list.get(1) << endl; 
    cout << "O numero 2 esta na posicao: " << list.get(2) << endl;
    cout << "O numero 3 esta na posicao: " << list.get(3) << endl;
    cout << "O numero 4 esta na posicao: " << list.get(4) << endl;
    cout << "O numero 5 esta na posicao: " << list.get(5) << endl;
    cout << "O numero 6 esta na posicao: " << list.get(6) << endl;
    cout << "O numero 7 esta na posicao: " << list.get(7) << endl;
    cout << "O numero 8 esta na posicao: " << list.get(8) << endl;
    cout << "O numero 9 esta na posicao: " << list.get(9) << endl;
    cout << "O numero 10 esta na posicao: " << list.get(10) << endl;
    cout << "O numero 11 esta na posicao: " << list.get(11) << endl;
    cout << "O numero 12 esta na posicao: " << list.get(12) << endl;
    cout << "O numero 13 esta na posicao: " << list.get(13) << endl;
    cout << "O numero 14 esta na posicao: " << list.get(14) << endl;
    cout << "O numero 15 esta na posicao: " << list.get(15) << endl;

    cout << endl << "O tamanho da lista e: " << list.size() << endl << endl;

    list.add(16, 0);
    list.add(17, 0);
    list.add(18, 0);
    list.add(19, 0);
    list.add(20, 0);

    cout << endl << "O numero 1 esta na posicao: " << list.get(1) << endl; 
    cout << "O numero 2 esta na posicao: " << list.get(2) << endl;
    cout << "O numero 3 esta na posicao: " << list.get(3) << endl;
    cout << "O numero 4 esta na posicao: " << list.get(4) << endl;
    cout << "O numero 5 esta na posicao: " << list.get(5) << endl;
    cout << "O numero 6 esta na posicao: " << list.get(6) << endl;
    cout << "O numero 7 esta na posicao: " << list.get(7) << endl;
    cout << "O numero 8 esta na posicao: " << list.get(8) << endl;
    cout << "O numero 9 esta na posicao: " << list.get(9) << endl;
    cout << "O numero 10 esta na posicao: " << list.get(10) << endl;
    cout << "O numero 11 esta na posicao: " << list.get(11) << endl;
    cout << "O numero 12 esta na posicao: " << list.get(12) << endl;
    cout << "O numero 13 esta na posicao: " << list.get(13) << endl;
    cout << "O numero 14 esta na posicao: " << list.get(14) << endl;
    cout << "O numero 15 esta na posicao: " << list.get(15) << endl;
    cout << "O numero 16 esta na posicao: " << list.get(16) << endl;
    cout << "O numero 17 esta na posicao: " << list.get(17) << endl;
    cout << "O numero 18 esta na posicao: " << list.get(18) << endl;
    cout << "O numero 19 esta na posicao: " << list.get(19) << endl;
    cout << "O numero 20 esta na posicao: " << list.get(20) << endl;

    cout << endl << "O tamanho da lista e: " << list.size() << endl << endl;

    list.inverter();

    cout << endl << "O numero 1 esta na posicao: " << list.get(1) << endl; 
    cout << "O numero 2 esta na posicao: " << list.get(2) << endl;
    cout << "O numero 3 esta na posicao: " << list.get(3) << endl;
    cout << "O numero 4 esta na posicao: " << list.get(4) << endl;
    cout << "O numero 5 esta na posicao: " << list.get(5) << endl;
    cout << "O numero 6 esta na posicao: " << list.get(6) << endl;
    cout << "O numero 7 esta na posicao: " << list.get(7) << endl;
    cout << "O numero 8 esta na posicao: " << list.get(8) << endl;
    cout << "O numero 9 esta na posicao: " << list.get(9) << endl;
    cout << "O numero 10 esta na posicao: " << list.get(10) << endl;
    cout << "O numero 11 esta na posicao: " << list.get(11) << endl;
    cout << "O numero 12 esta na posicao: " << list.get(12) << endl;
    cout << "O numero 13 esta na posicao: " << list.get(13) << endl;
    cout << "O numero 14 esta na posicao: " << list.get(14) << endl;
    cout << "O numero 15 esta na posicao: " << list.get(15) << endl;
    cout << "O numero 16 esta na posicao: " << list.get(16) << endl;
    cout << "O numero 17 esta na posicao: " << list.get(17) << endl;
    cout << "O numero 18 esta na posicao: " << list.get(18) << endl;
    cout << "O numero 19 esta na posicao: " << list.get(19) << endl;
    cout << "O numero 20 esta na posicao: " << list.get(20) << endl;

    cout << endl << "O tamanho da lista e: " << list.size() << endl << endl;*/


        
    ///****** EXEMPLO 3 = ADICIONANDO SEMPRE NAS POSIÇÕES 0, 1, 2, 3 E 4 DA LISTA ******///

   /* list.add(1, 0); //adiciona um elemento na lista
	list.add(2, 1);
	list.add(3, 2);
    list.add(4, 3);
    list.add(5, 4);
    list.add(6, 0);
    list.add(7, 1);
    list.add(8, 2);
    list.add(9, 3);
    list.add(10, 4);


	cout << endl << "O numero 1 esta na posicao: " << list.get(1) << endl; // retorna a posição do elemento que foi passado por parâmetro
    cout << "O numero 2 esta na posicao: " << list.get(2) << endl;
    cout << "O numero 3 esta na posicao: " << list.get(3) << endl;
    cout << "O numero 4 esta na posicao: " << list.get(4) << endl;
    cout << "O numero 5 esta na posicao: " << list.get(5) << endl;
    cout << "O numero 6 esta na posicao: " << list.get(6) << endl;
    cout << "O numero 7 esta na posicao: " << list.get(7) << endl;
    cout << "O numero 8 esta na posicao: " << list.get(8) << endl;
    cout << "O numero 9 esta na posicao: " << list.get(9) << endl;
    cout << "O numero 10 esta na posicao: " << list.get(10) << endl;

    cout << endl << "O tamanho da lista e: " << list.size() << endl << endl;    //retorna o tamanho da lista

    list.inverter();                                                            //inverte de tras para frente a lista

    cout << endl << "O numero 1 esta na posicao: " << list.get(1) << endl; 
    cout << "O numero 2 esta na posicao: " << list.get(2) << endl;
    cout << "O numero 3 esta na posicao: " << list.get(3) << endl;
    cout << "O numero 4 esta na posicao: " << list.get(4) << endl;
    cout << "O numero 5 esta na posicao: " << list.get(5) << endl;
    cout << "O numero 6 esta na posicao: " << list.get(6) << endl;
    cout << "O numero 7 esta na posicao: " << list.get(7) << endl;
    cout << "O numero 8 esta na posicao: " << list.get(8) << endl;
    cout << "O numero 9 esta na posicao: " << list.get(9) << endl;
    cout << "O numero 10 esta na posicao: " << list.get(10) << endl;

    cout << endl << "O tamanho da lista e: " << list.size() << endl << endl;

    list.inverter();            

    cout << endl << "O numero 1 esta na posicao: " << list.get(1) << endl;  
    cout << "O numero 2 esta na posicao: " << list.get(2) << endl;
    cout << "O numero 3 esta na posicao: " << list.get(3) << endl;
    cout << "O numero 4 esta na posicao: " << list.get(4) << endl;
    cout << "O numero 5 esta na posicao: " << list.get(5) << endl;
    cout << "O numero 6 esta na posicao: " << list.get(6) << endl;
    cout << "O numero 7 esta na posicao: " << list.get(7) << endl;
    cout << "O numero 8 esta na posicao: " << list.get(8) << endl;
    cout << "O numero 9 esta na posicao: " << list.get(9) << endl;
    cout << "O numero 10 esta na posicao: " << list.get(10) << endl;

    cout << endl << "O tamanho da lista e: " << list.size() << endl << endl;

    list.add(11, 0);
    list.add(12, 1);
    list.add(13, 2);
    list.add(14, 3);
    list.add(15, 4);

    cout << endl << "O numero 1 esta na posicao: " << list.get(1) << endl;  
    cout << "O numero 2 esta na posicao: " << list.get(2) << endl;
    cout << "O numero 3 esta na posicao: " << list.get(3) << endl;
    cout << "O numero 4 esta na posicao: " << list.get(4) << endl;
    cout << "O numero 5 esta na posicao: " << list.get(5) << endl;
    cout << "O numero 6 esta na posicao: " << list.get(6) << endl;
    cout << "O numero 7 esta na posicao: " << list.get(7) << endl;
    cout << "O numero 8 esta na posicao: " << list.get(8) << endl;
    cout << "O numero 9 esta na posicao: " << list.get(9) << endl;
    cout << "O numero 10 esta na posicao: " << list.get(10) << endl;
    cout << "O numero 11 esta na posicao: " << list.get(11) << endl;
    cout << "O numero 12 esta na posicao: " << list.get(12) << endl;
    cout << "O numero 13 esta na posicao: " << list.get(13) << endl;
    cout << "O numero 14 esta na posicao: " << list.get(14) << endl;
    cout << "O numero 15 esta na posicao: " << list.get(15) << endl;

    cout << endl << "O tamanho da lista e: " << list.size() << endl << endl;

    list.inverter();

    cout << endl << "O numero 1 esta na posicao: " << list.get(1) << endl; 
    cout << "O numero 2 esta na posicao: " << list.get(2) << endl;
    cout << "O numero 3 esta na posicao: " << list.get(3) << endl;
    cout << "O numero 4 esta na posicao: " << list.get(4) << endl;
    cout << "O numero 5 esta na posicao: " << list.get(5) << endl;
    cout << "O numero 6 esta na posicao: " << list.get(6) << endl;
    cout << "O numero 7 esta na posicao: " << list.get(7) << endl;
    cout << "O numero 8 esta na posicao: " << list.get(8) << endl;
    cout << "O numero 9 esta na posicao: " << list.get(9) << endl;
    cout << "O numero 10 esta na posicao: " << list.get(10) << endl;
    cout << "O numero 11 esta na posicao: " << list.get(11) << endl;
    cout << "O numero 12 esta na posicao: " << list.get(12) << endl;
    cout << "O numero 13 esta na posicao: " << list.get(13) << endl;
    cout << "O numero 14 esta na posicao: " << list.get(14) << endl;
    cout << "O numero 15 esta na posicao: " << list.get(15) << endl;

    cout << endl << "O tamanho da lista e: " << list.size() << endl << endl;

    list.add(16, 0);
    list.add(17, 1);
    list.add(18, 2);
    list.add(19, 3);
    list.add(20, 4);

    cout << endl << "O numero 1 esta na posicao: " << list.get(1) << endl; 
    cout << "O numero 2 esta na posicao: " << list.get(2) << endl;
    cout << "O numero 3 esta na posicao: " << list.get(3) << endl;
    cout << "O numero 4 esta na posicao: " << list.get(4) << endl;
    cout << "O numero 5 esta na posicao: " << list.get(5) << endl;
    cout << "O numero 6 esta na posicao: " << list.get(6) << endl;
    cout << "O numero 7 esta na posicao: " << list.get(7) << endl;
    cout << "O numero 8 esta na posicao: " << list.get(8) << endl;
    cout << "O numero 9 esta na posicao: " << list.get(9) << endl;
    cout << "O numero 10 esta na posicao: " << list.get(10) << endl;
    cout << "O numero 11 esta na posicao: " << list.get(11) << endl;
    cout << "O numero 12 esta na posicao: " << list.get(12) << endl;
    cout << "O numero 13 esta na posicao: " << list.get(13) << endl;
    cout << "O numero 14 esta na posicao: " << list.get(14) << endl;
    cout << "O numero 15 esta na posicao: " << list.get(15) << endl;
    cout << "O numero 16 esta na posicao: " << list.get(16) << endl;
    cout << "O numero 17 esta na posicao: " << list.get(17) << endl;
    cout << "O numero 18 esta na posicao: " << list.get(18) << endl;
    cout << "O numero 19 esta na posicao: " << list.get(19) << endl;
    cout << "O numero 20 esta na posicao: " << list.get(20) << endl;

    cout << endl << "O tamanho da lista e: " << list.size() << endl << endl;

    list.inverter();

    cout << endl << "O numero 1 esta na posicao: " << list.get(1) << endl; 
    cout << "O numero 2 esta na posicao: " << list.get(2) << endl;
    cout << "O numero 3 esta na posicao: " << list.get(3) << endl;
    cout << "O numero 4 esta na posicao: " << list.get(4) << endl;
    cout << "O numero 5 esta na posicao: " << list.get(5) << endl;
    cout << "O numero 6 esta na posicao: " << list.get(6) << endl;
    cout << "O numero 7 esta na posicao: " << list.get(7) << endl;
    cout << "O numero 8 esta na posicao: " << list.get(8) << endl;
    cout << "O numero 9 esta na posicao: " << list.get(9) << endl;
    cout << "O numero 10 esta na posicao: " << list.get(10) << endl;
    cout << "O numero 11 esta na posicao: " << list.get(11) << endl;
    cout << "O numero 12 esta na posicao: " << list.get(12) << endl;
    cout << "O numero 13 esta na posicao: " << list.get(13) << endl;
    cout << "O numero 14 esta na posicao: " << list.get(14) << endl;
    cout << "O numero 15 esta na posicao: " << list.get(15) << endl;
    cout << "O numero 16 esta na posicao: " << list.get(16) << endl;
    cout << "O numero 17 esta na posicao: " << list.get(17) << endl;
    cout << "O numero 18 esta na posicao: " << list.get(18) << endl;
    cout << "O numero 19 esta na posicao: " << list.get(19) << endl;
    cout << "O numero 20 esta na posicao: " << list.get(20) << endl;

    cout << endl << "O tamanho da lista e: " << list.size() << endl << endl;*/


	list.~List(); // destrutor da classe

	return 0;
}