
#include "List.hpp"

List::List(){
	head = nullptr;
}

List::~List(){
	while(head != nullptr){
		Node * current = head;
		head = head->next;
		delete current;
	}
}

Node * List::getNode(int pos){
	Node * ant = nullptr;
	Node * current = head;
	int count = 0;
	while(current != nullptr){
		if(count >= pos){
			return ant;
		}
		count += 1;
		ant = current;
		current = current->next;
	}
	return ant;
}

// 0 1 2 3 4
//[a b c d]
bool List::add(int x, int pos){
	//caso 1: lista vazia
	//Criar o elemento
	//atualizar o head
	if(head == nullptr){
		if(pos == 0){
			Node * node = new Node(x);
			head = node;
			return true;
		}
	} else {
		int length = size();

		if(pos == 0){
			//caso 2: inserção na primeira posição (pos==0)
			//Criar o elemento x
			//x.proximo = head
			//head.anterior = x
			//head = x
			Node * node = new Node(x);
			node->next = head;
			head->previous = node;
			head = node;
			return true;

		} else if(pos==length){
			//caso 3: inserção na última posição (pos==size)
			//Criar o elemento x
			//Encontrar o último elemento c
			//c.proximo = x
			//x.anterior = c
			Node * node = new Node(x);
			
			Node * current = head;
			//[0 1 2]
			//     c
			// length = 3
			// i = 0, 1
			for(int i=0;i<length-1;i++){
				current = current->next;
			}
			
			current->next = node;
			node->previous = current;
			return true;
		} else if(pos > 0 && pos < length){
			//caso 4: inserção entre dois elementos
			//Verificar pos > 0 && pos < size
			//Encontrar o elemento na posição pos: c
			//Encontrar o elemento anterior ao pos: a
			//Criar o elemento x
			//x.proximo = c
			//x.anterior = a
			//a.proximo = x
			//c.anterior = x
			Node * current = head;
			for(int i=0;i<pos;i++){
				current = current->next;
			}
			Node * previous = current->previous;
			Node * node = new Node(x);
			
			node->next = current;
			node->previous = previous;
			previous->next = node;
			current->previous = node;
			return true;
		}	
	}
	return false;
}

int List::remove(int pos){
	if(head != nullptr){
		int length = size();
		if(length == 1 && pos == 0){
			//Caso 4: size() == 1
			//desalocar head
			//head = null
			int value = head->value;
			delete head;
			head = nullptr;
			return value;
		} else if(pos == 0){// ao menos dois elementos
			//caso 1: remoção do primeiro elemento
			//referência para o head: c
			//head = head.proximo
			//head.anterior = nulo
			//desalocar c
			Node * current = head;
			head = head->next;
			head->previous = nullptr;
			int value = current->value;
			delete current;
			return value;
		} else if(pos == length -1){ // ao menos dois elementos
			//caso 2: remoção do último elemento
			//verificar pos == size() -1
			//encontrar o último elemento c
			//anterior = c.anterior
			//anterior.proximo = nulo
			//desalocar o c
			Node * current = head;
			for(int i=0;i<length-1;i++){
				current = current->next;
			}
			Node * previous = current->previous;
			previous->next = nullptr;
			int value = current->value;
			delete current;
			return value;
		} else if(pos > 0 && pos < length -1){
			//caso 3: remoção entre dois elementos
			//Verificar se pos > 0 && pos < size() -1
			//Encontrar o elemento c na posição pos
			//Determinar o elemento seguinte s: s = c.proximo
			//Determinar o elemento anterior a: a = c.anterior
			// a.proximo = s
			// s.anterior = a
			// desalocar o c
			
			Node * current = head;
			for(int i=0;i<pos;i++){
				current = current->next;
			}
			Node * next = current->next;
			Node * previous = current->previous;
			previous->next = next;
			next->previous = previous;
			int value = current->value;
			delete current;
			return value;
		}
	}
	return -1;
}

int List::size(){
	int count = 0;
	Node * current = head;
	while(current != nullptr){
		current = current->next;
		count += 1;
	}
	return count;
}

int List::get(int x){
	if(head == nullptr){
		return -1;
	} else {
		int count = 0;
		Node * current = head;
		while(current != nullptr){
			if(x == current->value){
				return count;
			}
			count += 1;
			current = current->next;
		}
		return -1;
	}
}

void List::inverter(){
	int tamanho = size();

	 if (tamanho > 0)
	 {
			 Node * auxTail = getNode(tamanho);
			 Node * auxCurrent = head;
			 int aux;
		 if (tamanho % 2 == 0)
		 {
			 for (int i = 0; i < (tamanho/2); i++)
			 {				 
					aux = auxCurrent->value;
					auxCurrent->value = auxTail->value;
					auxTail->value = aux;


					auxCurrent = auxCurrent->next;
					auxTail = auxTail->previous;

			 }
			 
		 }else
		 {
			 for (int i = 0; i < ((tamanho-1)/2); i++)
			 {

				 	aux = auxCurrent->value;
					auxCurrent->value = auxTail->value;
					auxTail->value = aux;


					auxCurrent = auxCurrent->next;
					auxTail = auxTail->previous;
			 }
		 }
		 
		 
	 }else
	 {
		 cout << endl << "[ERRO!!!] A lista não possui elementos! " << endl << endl;
	 }
	 
	 

}
