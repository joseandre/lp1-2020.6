
#ifndef NODE_HPP_
#define NODE_HPP_


class Node {
	public:
		int value;			//atributo que guarda o valor do no
		Node * next;		//atributo que armazena qual é o próximo nó
		Node * previous;	//atributo que armazena qual é o nó anterior
		
		Node(int v);		//construtor da classe
};


#endif
