#include "deck.hpp"
#include <iostream>

using namespace std;

Deck::Deck() {
	head = nullptr;
	tail = nullptr;
	tamanho = 0;

}

Deck::~Deck() {
	while(head != nullptr){
		Node* current = head;
		head = head->next;
		delete current;
	}
    delete head;
}

void Deck::set_head(int valor) {
	
	Node* newNode = new Node(valor);

	if (tamanho == 0)
	{
		head = newNode;
		tail = newNode;
		tamanho++;

	}else
	{
		newNode->next = head;
		head->previous = newNode;
		head = newNode;
		tamanho++;
		
	}
	return;
}

void Deck::set_tail(int valor){

	Node* newNode = new Node(valor);

	if (tamanho == 0)
	{
		head = newNode;
		tail = newNode;
		tamanho++;

	}else	
	{
		newNode->previous = tail;
		tail->next = newNode;
		tail = newNode;
		tamanho++;
	}
	
	
	return;
}

int Deck::get_tamanho(){
	if (tamanho >= 0)
	{
	return tamanho;
	}else{
		cout << "[ERRO!!!]   Não existe elementos na lista. " << endl;
		return -1;
	}
	
}

int Deck::get_head(){
	if (tamanho > 0)
	{
		return head->value;

	}else{
		cout << "[ERRO!!!]   Não existe elementos na lista. " << endl;
		return -1;
	}
}

int Deck::get_tail(){
	if (tamanho > 0)
	{
		return tail->value;

	}else{
		cout << "[ERRO!!!]   Não existe elementos na lista. " << endl;
		return -1;
	}
}

int Deck::remover_head(){
	if(tamanho == 0){
		cout << "[ERRO!!!]   Não existe elementos na lista. " << endl;

		return -1;
	}else if (tamanho == 1)
	{
		int aux = head->value;
		delete head;
		delete tail;

		tamanho--;
		
		return aux;
	}else
	{
		int aux = head->value;
		Node* newHead = head->next;

		delete head;

		head = newHead;

		tamanho--;

		return aux;

	}
}

int Deck::remover_tail(){
	if(tamanho == 0){
		cout << "[ERRO!!!]   Não existe elementos na lista. " << endl;

		return -1;
	}else if (tamanho == 1)
	{
		int aux = tail->value;
		delete head;
		delete tail;

		tamanho--;
		
		return aux;
	}else
	{
		int aux = tail->value;
		Node* newTail = tail->previous;

		delete tail;

		tail = newTail;

		tamanho--;

		return aux;

	}

}
