/*
	* O algoritmo Deck foi feito baseado na estrutura de lista duplamente encadeada.
	* O algoritmo nos possibilita fazer adições e remoções de elementos no inicio e no fim da lista.
	* Para fazer as adições eu criei um método para adicionar no inicio e outro para adicionar no fim.
	* Para fazer as remoções eu crei um método para remover no inicio e outro para remover no fim.
	* Criei um método para retornar o elemento do inicio e outro para retornar o elemento do fim.
	* Por fim eu também criei um método para retornar o tamanho do Deck.
	* Comentei o que cada método faz nos arquivos .hpp e aqui na main.
	* Fiz 3 exemplos que eram pedidos na questão, apenas o exemplo 1 está descomentado, para compilar os outros basta descomentar eles.
*/

#include <iostream>
#include "deck.hpp"

using namespace std;

int main (){
	Deck deck1;
	Deck deck2;
	Deck deck3;


    ///****** EXEMPLO 1 ******///

	deck1.set_head(1); //adiciona um elemento no inicio da lista
	deck1.set_head(2);

	cout << "Removendo do inicio: " << deck1.remover_head() << endl; // remove o elemento que está no topo da deck1

	cout << endl << "O tamanho do deck1 é: " << deck1.get_tamanho() << endl; //retorna o tamanho do deck1

	deck1.set_tail(1); //adiciona um elemento no fim da lista
	deck1.set_tail(2);

	cout << endl << "Removendo do fim: " << deck1.remover_tail() << endl; // remove o elemento que está no topo da deck1

	cout << endl << "O tamanho do deck1 é: " << deck1.get_tamanho() << endl; 

	cout << endl << "Removendo do inicio: " << deck1.remover_head() << endl;
	cout << "Removendo do inicio: " << deck1.remover_head() << endl;

	cout << endl << "O tamanho do deck1 é: " << deck1.get_tamanho() << endl; 

	deck1.set_head(1); 
	deck1.set_head(2);

	cout << endl << "O tamanho do deck1 é: " << deck1.get_tamanho() << endl; 

	cout << endl << "Removendo do fim: " << deck1.remover_tail() << endl;
	cout << "Removendo do fim: " << deck1.remover_tail() << endl;

	cout << endl << "O tamanho do deck1 é: " << deck1.get_tamanho() << endl; 

	deck1.set_tail(1); 
	deck1.set_tail(2);

	cout << endl << "O tamanho do deck1 é: " << deck1.get_tamanho() << endl; 

	deck1.set_head(1); 
	deck1.set_tail(2);

	cout << endl << "O tamanho do deck1 é: " << deck1.get_tamanho() << endl; 

	cout << endl << "O inicio do deck1 é: " << deck1.get_head() << endl; //retorna a cabeça do deck1
	
	cout << "O fim do deck1 é: " << deck1.get_tail() << endl << endl; //retorna o tail do deck1


	deck1.~Deck(); // destrutor da classe
	


	///****** EXEMPLO 2 ******///

/*	deck2.set_head(1); //adiciona um elemento no inicio da lista
	deck2.set_head(2);
	deck2.set_tail(4);

	cout << "Removendo do inicio: " << deck2.remover_head() << endl; // remove o elemento que está no topo da deck2
	cout << "Removendo do inicio: " << deck2.remover_head() << endl;

	cout << endl << "O tamanho do deck2 é: " << deck2.get_tamanho() << endl; //retorna o tamanho do deck2

	deck2.set_tail(1); //adiciona um elemento no fim da lista
	deck2.set_tail(2);
	deck2.set_head(4);

	cout << endl << "Removendo do fim: " << deck2.remover_tail() << endl; // remove o elemento que está no topo da deck2
	cout << endl << "Removendo do fim: " << deck2.remover_tail() << endl; 

	cout << endl << "O tamanho do deck2 é: " << deck2.get_tamanho() << endl; 

	cout << endl << "Removendo do inicio: " << deck2.remover_head() << endl;
	cout << "Removendo do inicio: " << deck2.remover_head() << endl;

	cout << endl << "O tamanho do deck2 é: " << deck2.get_tamanho() << endl; 

	deck2.set_head(10); 
	deck2.set_head(12);
	deck2.set_tail(20);

	cout << endl << "O tamanho do deck2 é: " << deck2.get_tamanho() << endl; 

	cout << endl << "Removendo do fim: " << deck2.remover_tail() << endl;
	cout << "Removendo do fim: " << deck2.remover_tail() << endl;
	cout << "Removendo do inicio: " << deck2.remover_head() << endl;

	cout << endl << "O tamanho do deck2 é: " << deck2.get_tamanho() << endl; 

	deck2.set_tail(1); 
	deck2.set_tail(2);

	cout << endl << "O tamanho do deck2 é: " << deck2.get_tamanho() << endl; 

	deck2.set_head(1); 
	deck2.set_tail(2);

	cout << endl << "O tamanho do deck2 é: " << deck2.get_tamanho() << endl; 

	cout << endl << "O inicio do deck2 é: " << deck2.get_head() << endl; //retorna a cabeça do deck2
	
	cout << "O fim do deck2 é: " << deck2.get_tail() << endl << endl; //retorna o tail do deck2


	deck2.~Deck(); // destrutor da classe
	*/


	///****** EXEMPLO 3 ******///

	/*deck3.set_head(10); //adiciona um elemento no inicio da lista
	deck3.set_head(52);
	deck3.set_tail(49);

	cout << "Removendo do inicio: " << deck3.remover_head() << endl; // remove o elemento que está no topo da deck3
	cout << "Removendo do inicio: " << deck3.remover_head() << endl;

	cout << endl << "O tamanho do deck3 é: " << deck3.get_tamanho() << endl; //retorna o tamanho do deck3

	deck3.set_tail(11); //adiciona um elemento no fim da lista
	deck3.set_tail(23);
	deck3.set_head(84);

	cout << endl << "Removendo do fim: " << deck3.remover_tail() << endl; // remove o elemento que está no topo da deck3
	cout << endl << "Removendo do fim: " << deck3.remover_tail() << endl; 

	cout << endl << "O tamanho do deck3 é: " << deck3.get_tamanho() << endl; 

	cout << endl << "Removendo do inicio: " << deck3.remover_head() << endl;
	cout << "Removendo do inicio: " << deck3.remover_head() << endl;

	cout << endl << "O tamanho do deck3 é: " << deck3.get_tamanho() << endl; 

	deck3.set_head(100); 
	deck3.set_head(120);
	deck3.set_tail(200);

	cout << endl << "O tamanho do deck3 é: " << deck3.get_tamanho() << endl; 

	cout << endl << "Removendo do fim: " << deck3.remover_tail() << endl;
	cout << "Removendo do fim: " << deck3.remover_tail() << endl;
	cout << "Removendo do inicio: " << deck3.remover_head() << endl;

	cout << endl << "O tamanho do deck3 é: " << deck3.get_tamanho() << endl; 

	deck3.set_tail(19); 
	deck3.set_tail(29);

	cout << endl << "O tamanho do deck3 é: " << deck3.get_tamanho() << endl; 

	deck3.set_head(15); 
	deck3.set_tail(25);

	cout << endl << "O tamanho do deck3 é: " << deck3.get_tamanho() << endl; 

	cout << endl << "O inicio do deck3 é: " << deck3.get_head() << endl; //retorna a cabeça do deck3
	
	cout << "O fim do deck3 é: " << deck3.get_tail() << endl << endl; //retorna o tail do deck3


	deck3.~Deck(); // destrutor da classe
*/
	return 0;
}