#ifndef PILHA_HPP
#define PILHA_HPP

#include <iostream>
#include "Node.hpp"

using namespace std;


class Deck {
	int tamanho;					//atributo que armazena o tamanho do deck
	Node * head;					//atributo que armazena o primeiro elemento
	Node * tail;					//atributo que armazena o último elemento
	
	public:
	
	Deck();							//construtor da classe
	~Deck();						//destrutor da classe
	void set_head(int valor);		//método que adiciona elementos no inicio
	void set_tail(int valor);		//método que adiciona elementos no fim
	int get_head();					//método que retorna o elemento do inicio
	int get_tail();					//método que retorna o elemento do fim
	int get_tamanho();				//método que retorna o tamanho (quantidade de elementos) do deck
	int remover_head();				//método que remove o elemento do inicio
	int remover_tail();				//método que remove o elemento do fim
};
#endif