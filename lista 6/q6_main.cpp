#include <iostream>
#include "Queue.hpp"

using namespace std;

int main(){

    Queue lista;

    lista.add(2);
    lista.add(0);
    lista.add(1);
    lista.add(8);
    lista.add(0);
    lista.add(0);
    lista.add(0);
    lista.add(0);
    lista.add(9);
    lista.add(6);
    lista.add(2);
    
    cout << endl << "A lista tem tamanho: " << lista.size() << endl << endl;

    cout << endl << "O primeiro elemento é: " << lista.get() << endl << endl;


    cout << endl << "O elemento removido é: " << lista.remove() << endl;
    cout << endl << "O elemento removido é: " << lista.remove() << endl;
    cout << endl << "O elemento removido é: " << lista.remove() << endl;

    cout << endl << endl << "Agora a lista tem tamanho: " << lista.size() << endl << endl;

    cout << endl << "Agora o primeiro elemento é: " << lista.get() << endl << endl;

    return 0;
}