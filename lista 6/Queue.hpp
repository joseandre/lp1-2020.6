#ifndef Queue_HPP_
#define Queue_HPP_

#include "Node.hpp"

class Queue {
	Node * head;
	Node * tail; // ultimo elemento
	
	public:
	
		Queue();
		~Queue();
		bool add(int x); // adiciona um elemento no final da fila
		int remove(); // remove o primeiro elemento inserido na fila 
		int get(); // recupera, mas não remove, o primeiro elemento inserido na fila
		int size(); // retorna a quantidade de elementos na fila
};

#endif