#include <cstddef>
#include <iostream>
#include "Queue.hpp"

using namespace std;


Queue::Queue(){
    head = NULL;
	tail = NULL;
}

Queue::~Queue(){
    while(head != NULL){
		Node* current = head;
		head = head->next;
		delete current;
	}
    delete head;
}

bool Queue::add(int x){
    Node* newNode = new Node(x);

	if (head == NULL)
	{
		head = newNode;
		tail = newNode;

	}else 
	{
		tail->next = newNode;
		tail = newNode;

	}

	return true;
}

int Queue::remove(){
    if(head == NULL){
		cout << "[ERRO!!!]   Não existe elementos na lista. " << endl;

		return 0;
	}

	//cout << "Elemento " << head->value << " removido da head." << endl;
    int aux1 = head->value;
    Node*  aux2 = head;

	head = head->next;

    delete aux2;

	return aux1;
}

int Queue::get(){
    return head->value;
}

int Queue::size(){
    int count = 0;
    
    Node* current = head;
	while(current != NULL){
		current = current->next;
		count += 1;
	}
	return count;
}