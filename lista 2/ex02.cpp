//Bubble Sort

#include <iostream>

using namespace std;

void bubble_Sort(int vet[], int tam){

    int n = tam;
    while ((n-1) > 0){
        int aux;

        for (int i = 0; i < n; i++){
            if (vet[i] > vet[i+1]){
            aux = vet[i];
            vet[i] = vet[i+1];
            vet[i+1] = aux;
            }
        }
        
        n -= 1;
    }

    
    cout << "Vetor organizado:";
    for (int i = 0; i < tam; i++){
        cout << " " << vet[i];
    }
    cout << endl << endl;

}

int main()
{
    int n;
    cout << "Digite o tamanho do vetor: ";
    cin >> n;
    
   int s[n];
    
    for (int i = 0; i < n; i++){
    
    cout << "Digite os elemento " << i << " do vetor: ";
    cin >> s[i];
    }

    cout << endl << endl;
    cout << "Vetor inicialmente:";
    for (int i = 0; i < n; i++){
        cout << " " << s[i];
    }
    cout << endl << endl;


    bubble_Sort (s, n);
    return 0;
}
