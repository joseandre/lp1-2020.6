//Quick Sort

#include <iostream>

using namespace std;

void quick_Sort(int vet[], int ini, int fim)
{
	int pivo, pont_esq, pont_dir, aux;
	
  pont_esq = ini;
	pont_dir = fim - 1;
	pivo = vet[(ini + fim) / 2];

	while(pont_esq <= pont_dir){
		while(vet[pont_esq] < pivo && pont_esq < fim){
			pont_esq++;
		}
		while(vet[pont_dir] > pivo && pont_dir > ini){
			pont_dir--;
		}
		if(pont_esq <= pont_dir){
			aux = vet[pont_esq];
			vet[pont_esq] = vet[pont_dir];
			vet[pont_dir] = aux;
			pont_esq++;
			pont_dir--;
		}
	}
	if(pont_dir > ini){
		quick_Sort(vet, ini, pont_dir+1);
  }
	if(pont_esq < fim){
		quick_Sort(vet, pont_esq, fim);
  }
}

int main(){
    int n;
    cout << endl << "Digite o tamanho do vetor: ";
    cin >> n;
    
    int s[n];
    
    for (int i = 0; i < n; i++){
    
    cout << "Digite o elemento " << i << " do vetor: ";
    cin >> s[i];
    }

    cout << endl << endl;
    cout << "Vetor inicialmente:";
    for (int i = 0; i < n; i++){
        cout << " " << s[i];
    }
    cout << endl << endl;

    quick_Sort(s, 0, n);
    
    cout << "Vetor organizado: ";
    for(int i = 0; i < n; i++) {
        cout << s[i] << " ";
    }

cout << endl << endl;
    return 0;
}