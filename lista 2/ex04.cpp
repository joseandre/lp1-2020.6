//Marge Sort

#include <iostream>

using namespace std;

void ragrupar(int vet[], int ini, int meio, int fim, int vetAux[]) {
    int esq = ini;
    int dir = meio;

    for (int i = ini; i < fim; ++i) {
        if ((esq < meio) && ((dir >= fim) || (vet[esq] < vet[dir]))) {
            vetAux[i] = vet[esq];
            ++esq;
        }
        else {
            vetAux[i] = vet[dir];
            ++dir;
        }
    }
    for (int i = ini; i < fim; ++i) {
        vet[i] = vetAux[i];
    }
}

void marge_Sort(int vet[], int ini, int fim, int vet_Aux[]) {
    if ((fim - ini) < 2) return;
    
    int meio = ((ini + fim)/2);
    marge_Sort(vet, ini, meio, vet_Aux);
    marge_Sort(vet, meio, fim, vet_Aux);
    ragrupar(vet, ini, meio, fim, vet_Aux);
}

void marge_Sort(int vet[], int tam) { 
    int vet_Aux[tam];
    marge_Sort(vet, 0, tam, vet_Aux);
}


int main(){
    int n;
    cout << endl << "Digite o tamanho do vetor: ";
    cin >> n;
    
    int s[n];
    
    for (int i = 0; i < n; i++){
    
    cout << "Digite o elemento " << i << " do vetor: ";
    cin >> s[i];
    }

    cout << endl << endl;
    cout << "Vetor inicialmente:";
    for (int i = 0; i < n; i++){
        cout << " " << s[i];
    }
    cout << endl << endl;

    marge_Sort(s, n);
    
    cout << "Vetor organizado: ";
    for(int i = 0; i < n; i++) {
        cout << s[i] << " ";
    }

cout << endl << endl;
    return 0;
}