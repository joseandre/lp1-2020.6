//Selection Sort

#include <iostream>

using namespace std;

void selection_Sort(int vet[], int tam)
{
    int i, j, min, aux;

    for (i = 0; i < (tam - 1); i++){
        min = i;
        for (j = (i + 1); j < tam; j++){
            if (vet[j] < vet[min])
                min = j;
        }
        if (vet[i] != vet[min]){
            aux = vet[i];
            vet[i] = vet[min];
            vet[min] = aux;
        }
    }

    cout << " Vetor organizado: ";
    for (int i = 0; i < tam; i++)
    {
        cout << vet [i] << "  ";
    }
}

int main()
{
    int n;
    cout << "Digite o tamanho do vetor: ";
    cin >> n;
    
    int s[n];
    
    for (int i = 0; i < n; i++){
    
    cout << "Digite os elemento " << i << " do vetor: ";
    cin >> s[i];
    }

    cout << endl << endl;
    cout << "Vetor inicialmente:";
    for (int i = 0; i < n; i++){
        cout << " " << s[i];
    }
    cout << endl << endl;

    selection_Sort(s, n);

    return 0;
}