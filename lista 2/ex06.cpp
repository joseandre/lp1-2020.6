//Bubble Sort (questão 6)

#include <iostream>

using namespace std;

void bubble_Sort(int vet[],int tam, int ii, int jj){
    //int n = tam;
    while ((jj) > ii){
        int aux, ord = 0; 
        for (int i = ii; i < jj - 1; i++){
            if (vet[i] > vet[i+1]){
                ord = 1;
                aux = vet[i];
                vet[i] = vet[i+1];
                vet[i+1] = aux;
            }
        }
        if (ord == 0){
            break;
        }

        jj--;
    }
  
    cout << "Vetor organizado:";
    for (int i = 0; i < tam; i++){
        cout << " " << vet[i];
    }
    cout << endl << endl;

}

int main(){

    int n, ii, jj;

    cout << "Digite o tamanho do vetor: ";
    cin >> n;
    
    int s[n];

    cout << "Digite o valor de I (inclusive): ";
    cin >> ii;
    cout << "Digite o de J (exclusive): ";
    cin >> jj;
    
    for (int i = 0; i < n; i++){
    
    cout << "Digite os elemento " << i << " do vetor: ";
    cin >> s[i];
    }

    cout << endl << endl;
    cout << "Vetor inicialmente:";
    for (int i = 0; i < n; i++){
        cout << " " << s[i];
    }
    cout << endl << endl;

    bubble_Sort (s, n, ii, jj);
    return 0;
}
