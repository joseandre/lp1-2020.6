//Insert Sort

#include <iostream>

using namespace std;

void insert_Sort (int vet[], int tam){

    int aux;
    for (int i = 0; i < tam - 1; i++){
        if (vet[i+1] < vet[i])
        {
            for (int j = i+1; j > 0; j--)
            {
                if (vet[j] < vet[j-1] )
                {
                    aux = vet[j-1];
                    vet[j-1] = vet[j];
                    vet[j] = aux;
                }
                
            }
            
        }
        
    }
    cout << "Vetor organizado:";
    for (int i = 0; i < tam; i++){
        cout << " " << vet[i];
    }
    cout << endl << endl;
    
}

int main(){

    int n;
    cout << "Digite o tamanho do vetor: ";
    cin >> n;
    
   int s[n];
    
    for (int i = 0; i < n; i++){
    
    cout << "Digite os elemento " << i << " do vetor: ";
    cin >> s[i];
    }

    cout << endl << endl;
    cout << "Vetor inicialmente:";
    for (int i = 0; i < n; i++){
        cout << " " << s[i];
    }
    cout << endl << endl;

    insert_Sort (s, n);
    return 0;
}
