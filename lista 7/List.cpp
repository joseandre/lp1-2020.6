#include "List.hpp"

List::List(){
	head = nullptr;
//	tail = nullptr;

}

List::~List(){
	while(head != nullptr){
		Node * current = head;
		head = head->next;
		delete current;
	}
}

Node * List::getNode(int pos){
	Node * ant = nullptr;
	Node * current = head;
	int count = 0;
	while(current != nullptr){
		if(count >= pos){
			return ant;
		}
		count += 1;
		ant = current;
		current = current->next;
	}
	return ant;
}

bool List::add(int x, int pos){
	 Node* newNode = new Node(x);

	if (pos <= tamanho)
	{	
		Node * add = getNode(pos);

		if (add == nullptr)
		{
			head = newNode;
			//tail = newNode;
			tamanho++;

			return true;
		}
		
		newNode->next = add->next;
		newNode->previous = add;
		(add->next)->previous = newNode;
		add->next = newNode;

		tamanho++;

		return true;
	}
	return false;
}

int List::remove(int pos){
	if (head == nullptr)
	{
		return -1;

	}else 
	{
		if (pos <= tamanho && tamanho != 0)
		{
			Node * remove = getNode(pos);
			int deletado = remove->next->value;
			Node * deletando = remove->next;

			remove->next = remove->next->next;
			(remove->next->next)->previous = remove;

			tamanho--;

			delete deletando;

			return deletado;

		}

	}

	return -1;
}

int List::size(){
	int count = 0;
	Node * current = head;
	while(current != nullptr){
		current = current->next;
		count += 1;
	}
	return count;
}

int List::get(int x){
	if(head == nullptr){
		return -1;
	} else {
		int count = 0;
		Node * current = head;
		while(current != nullptr){
			if(x == current->value){
				return count;
			}
			count += 1;
			current = current->next;
		}
		return -1;
	}
}