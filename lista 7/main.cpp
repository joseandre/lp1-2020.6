#include "List.hpp"
#include <iostream>

using namespace std;

int main(){

    List lista;

    lista.add(2, 0);
    lista.add(0, 1);
    lista.add(1, 0);
    lista.add(8, 3);
    
    cout << endl << "A lista tem tamanho: " << lista.size() << endl << endl;

    cout << endl << "O elemento removido é: " << lista.remove(3) << endl;
            cout << endl << endl << "AQUIIII " << endl << endl;

    cout << endl << "O elemento removido é: " << lista.remove(1) << endl;
    cout << endl << "O elemento removido é: " << lista.remove(0) << endl;

    cout << endl << endl << "Agora a lista tem tamanho: " << lista.size() << endl << endl;

    return 0;
}
