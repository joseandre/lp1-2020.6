
#ifndef LIST_HPP_
#define LIST_HPP_

#include "Node.hpp"

class List {
	int tamanho = 0;
	Node * head;
	//Node * tail;				//adicionei esse atributo, mas não tive tempo de trabalhar com ele, ficará para implementações futuras.
	Node * getNode(int x);
	
	public:
		List();
		~List();
		
		bool add(int x, int pos);
		int remove(int pos);
		int get(int x);
		int size();
};

#endif