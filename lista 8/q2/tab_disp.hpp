#ifndef Q2TABDISP_HPP
#define Q2TABDISP_HPP

#include <iostream>

class Tab_disp {
	
	int n[30];							// tabela inicial
    int m[30];							// tabela dispersa
	
	public:
		Tab_disp();						// construtor da classe
		~Tab_disp();					// destrutor da classe
		
		void encadeando();				// método que passa os elementos para a tabela dispersa 
		void get(int num);				// recupera um elemento na tabela dispersa
        int gerarChave(int num);		// método que gera a chave (posição)
};

#endif