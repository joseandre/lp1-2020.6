#include <iostream>
#include "tab_disp.hpp"

using namespace std;

int main(){
    
    Tab_disp tab_disp;

    tab_disp.encadeando();

    cout << endl << endl;

    tab_disp.get(41);
    tab_disp.get(1);
    tab_disp.get(5705);
    tab_disp.get(6334);
    tab_disp.get(491);
    tab_disp.get(19718);

    cout << endl;

    return 0;
}