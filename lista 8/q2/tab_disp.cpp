#include "tab_disp.hpp"
#include <iostream>

using namespace std;


Tab_disp::Tab_disp(){
    unsigned int num = 3;
    n[0] = 1;
    m[0] = -1;

    for (int i = 1; i < 30; i++)
    {
        n[i] = rand();
        m[i] = -1;

        num = num * 2;
    }
    
}

Tab_disp::~Tab_disp(){

}

void Tab_disp::encadeando(){
    int posicao;

    for (int i = 0; i < 31; i++)
    {
        posicao = gerarChave(n[i]);

        for (int j = posicao; j < 31; j++)
        {        

            if (m[j] == -1)
            {
                m[j] = n[i];
                cout << endl << "O número " << n[i] << " foi guardado na posição: " << j << endl;

                break;
                
            }else if (j == 30)
            {
                j = -1;
            }else if (j+1 == posicao)
            {
                return;
            }

        }

    }
    
}

void Tab_disp::get(int num){

    int posicao = gerarChave(num);

    for (int i = posicao; i < 31; i++)
    {
        if (m[i] == num)
        {
            cout << endl << "O número " << num << " está na posição: " << i << endl;
            return;
        }else if (i == 30)
        {
            i = -1;
        }else if (i+1 == posicao)
        {
            return;
        }
        
    }
    
}

int Tab_disp::gerarChave(int num){
    return num % 15;
}


