#include <iostream>
#include "tab_disp.hpp"

using namespace std;

int main(){
    
    Tab_disp tab_disp;

    tab_disp.encadeando();

    cout << endl << endl;

    tab_disp.get(16);
    tab_disp.get(1);
    tab_disp.get(2);
    tab_disp.get(128);
    tab_disp.get(1024);
    tab_disp.get(65536);

    cout << endl;

    return 0;
}