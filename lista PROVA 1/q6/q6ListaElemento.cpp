#include <iostream>
#include <ostream>
#include "q6ListaElemento.hpp"

using namespace std;

ListaElemento::ListaElemento(){
    this->listaColuna = new Elemento[100];
    tamanho = 0;
}

/*ListaElemento::ListaElemento(int capacidadeColuna){
    this->listaColuna = new Elemento[capacidadeColuna];
    tamanho = 0;

}*/

ListaElemento::~ListaElemento(){
    delete[] listaColuna;
}

void ListaElemento::set_elemento(Elemento valor){

    listaColuna[tamanho].set_elemento(valor.indice, valor.valor); 

    tamanho++;
}

void ListaElemento::get_elementos(){
    for (int i = 0; i < tamanho; i++)
    {
        listaColuna[i].get_elemento();
    }
    
}