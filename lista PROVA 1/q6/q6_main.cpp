/*  
    *   Para resolver a questão eu criei 3 classes, onde um continha os atributos que guardavam o índice e o valor do elemento da matriz,
    * outra classe que tinha um ponteiro de elementos (ponteiro da classe citada anteriormente) que iria guardar uma lista dos elementos que eram
    * diferente de 0, nesse caso, esse vetor é uma linha da matriz. Por último  criei uma classe que continha um ponteiro de listas(ponteiro da
    *  classe citada anteriormente) assim ficou um ponteiro de listas, onde em cada índice desse ponteiro existia uma lista de elementos, assim formando 
    * uma matriz.
    *   Criei métodos para a inserção e impressão dos elementos que eram diferentes de 0. Comentei o que cada método faz em seus arquivos .hpp.
*/

#include <iostream>
#include "q6Matriz.hpp"

using namespace std;

int main(){
    int matriz[3][4] = {1, 0, 2, 0,
                        0, 3, 0, 4,
                        5, 0, 6, 0};

    cout << "--------- MATRIZ----------" << endl;
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            cout << matriz[i][j] << " ";

        }
        cout <<endl;
    }
    cout <<endl;

    Matriz listMatriz(matriz, 3, 4);

    listMatriz.get_elementos();
    
    return 0;
}