#ifndef Q6ELEMENTO_HPP
#define Q6ELEMENTO_HPP

class Elemento {
	
	public:

	int indice;										//atributos da classe
	int valor;
	
	Elemento(int indice_, int valor_);				//construtor da classe
	Elemento();										//construtor padrão da classe
	~Elemento();									//destrutor da classe
	void set_elemento(int indice_, int valor_);		//método para adicionar um elemento
	void get_elemento();							//método que imprime os elementos armazenados
};
#endif