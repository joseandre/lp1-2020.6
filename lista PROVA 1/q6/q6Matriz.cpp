#include <iostream>
#include "q6Matriz.hpp"
#include "q6ListaElemento.hpp"
#include "q6Elemento.hpp"

using namespace std;

Matriz::Matriz(int matriz_[][4], int linhas_, int colunas_){

    this->matriz = new ListaElemento[linhas_];
    linhas = linhas_;

    for (int i = 0; i < linhas_; i++)
    {
        for (int j = 0; j < colunas_; j++)
        {

            if ( matriz_[i][j] != 0)
            {
                Elemento posicao(j, matriz_[i][j]);
                matriz[i].set_elemento(posicao);
            }
            
        }
        
    }
    
}

Matriz::~Matriz(){
    for (int i = 0; i < linhas; i++)
    {
        matriz[i].~ListaElemento();
    }
    
    delete [] matriz;
}

void Matriz::get_elementos(){
    for (int i = 0; i < linhas; i++)
    {
        cout << "Na linha " << i << " temos os elementos: " << endl; 
        matriz[i].get_elementos();
        cout << endl;
    }
    
}