#include <iostream>
#include "q6Elemento.hpp"

using namespace std;

Elemento::Elemento(int indice_, int valor_){
    this->indice = indice_;
	this->valor = valor_;
}

Elemento::Elemento(){
    this->indice = -1;
	this->valor = -1;
}

Elemento::~Elemento(){

}

void Elemento::set_elemento(int indice_, int valor_){
    this->indice = indice_;
	this->valor = valor_;
}

void Elemento::get_elemento(){
    cout << "indice: " << indice << " valor: " << valor << endl;
}
