#ifndef Q6MATRIZ_HPP
#define Q6MATRIZ_HPP
#include "q6Elemento.hpp"
#include "q6ListaElemento.hpp"


class Matriz {

	ListaElemento* matriz;									//atributos da classe
	int linhas;
	
	public:
	
	Matriz( int matriz_[][4], int linha_, int colunas_);	//construtor da classe
	~Matriz();												//destrutor da classe
	void get_elementos();									//método para imprimir os elementos armazenados

};
#endif