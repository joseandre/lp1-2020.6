#ifndef Q6LISTAELEMENTO_HPP
#define Q6LISTAELEMENTO_HPP
#include "q6Elemento.hpp"


class ListaElemento {
	Elemento* listaColuna;				//atributos da classe
	int tamanho;
	
	public:
	
	ListaElemento();						//construtor padrão da classe
	//ListaElemento(int capacidadeColuna);	//construtor da classe
	~ListaElemento();						//destrutor da classe


	void set_elemento(Elemento valor);		//método para adicionar elementos
	void get_elementos();					//método para imprimir os elementos
	
};
#endif