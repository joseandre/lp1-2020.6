#ifndef PILHA_HPP
#define PILHA_HPP

class Pilha {
	int capacidade;
	int tamanho;
	int * elementos;
	
	public:
	
	Pilha(int capacidade);				//contrutor da classe
	~Pilha();							//destrutor da classe
	void set_elemento(int valor);		//método para adicionar elemento
	int get_Topo();						//método que retorna o topo da pilha
	int get_tamanho();					//método que retona o tamanho da pilha
	void get_elementos();				//método que imprime os elementos da pilha
	int get_valor(int valor);			//método que busca o elemento pelo valor, ele retorna o índice do primeiro elemento que ele achar que é igual o que foi passado por parâmetro
	int get_posicao(int pos);			//método que retorna o elemento que está na posição que foi passado por parâmetro
	void remover();						//método para desempilhar
};
#endif