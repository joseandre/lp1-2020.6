/*
    *   Utilizei a estrutura pilha e criei um objeto para os colchetes aberto, um para os fechados e um para as letras.
    *   Criei uma função para verificar os elementos do vetor e ir adicionando cada caractere em seu objeto correspondente, em seguida 
    * fiz comparações para saber se a quantidade de colchetes abertos menos a quantidade de colchetes fechados era igual a 0, se fosse, era porque
    * era uma palavra, mas perceba que logo no início eu coloquei um 'if' para verificar se o primeiro colchete era um colchete fechado.Tmabém 
    * faço uma comparação para saber se a quantidade de colchetes abertos e fechados eram 0, se fosse e tivesse letras, também seria uma palavra.
    *   Comentei o que cada método faz em seus arquivos .hpp.
*/

#include "q4Pilha.hpp"
#include <iostream>
#include <string.h>

using namespace std;

bool verificando(char* vetor_palavra)
{
    Pilha pilha_aberto(strlen(vetor_palavra));
    Pilha pilha_fechado(strlen(vetor_palavra));
    Pilha pilha_letra(strlen(vetor_palavra));


    char* aberto = "{";
    char* fechado = "}";

    if (vetor_palavra[0] == fechado[0])
    {
        return false;
    }

    for (int i = 0; i < strlen(vetor_palavra); i++)
    {
        if (vetor_palavra[i] == aberto[0])
        { 
            pilha_aberto.set_elemento(i);       //passando o índice de '{'
        }
        if (vetor_palavra[i] == fechado[0])
        {     
            pilha_fechado.set_elemento(i);      //passando o índice de '}'            
        }
        if ((vetor_palavra[i] != aberto[0]) && (vetor_palavra[i] != fechado[0]))
        {
            pilha_letra.set_elemento(i);        //passando o índice de alguma letra
        }
        
    }



    if ((pilha_aberto.get_tamanho() - pilha_fechado.get_tamanho()) == 0)
    {
        return true;

    }else if ((pilha_aberto.get_tamanho() == 0) && (pilha_fechado.get_tamanho() == 0))
    {
        if (pilha_letra.get_tamanho() > 0)
        {
            return true;
        }
        
    }else
    {
        return false;
    }

}

int main()
{
    string expressao;

    char vet_palavra1[] = "{{{}}}\0";
    char vet_palavra2[] = "}{\0";
    char vet_palavra3[] = "aaa\0";


    cout << endl << "O conjunto 1 ";
    if (verificando(vet_palavra1) == true){
        cout << "É uma palavra" << endl;
    }else{
        cout << "Não é palavra" << endl;
    }

    cout << endl << "O conjunto 2 ";
    if (verificando(vet_palavra2) == true){
        cout << "É uma palavra" << endl;
    }else{
        cout << "Não é palavra" << endl;
    }

    cout << endl << "O conjunto 3 ";
    if (verificando(vet_palavra3) == true){
        cout << "É uma palavra" << endl;
    }else{
        cout << "Não é palavra" << endl;
    }

}