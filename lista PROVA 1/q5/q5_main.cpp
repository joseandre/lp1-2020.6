//Bubble Sort

/*
    *   Utilizei o algoritmo do Bubble Sort 2.2 criado na lista 2.
    *   Reutilizei a função 'bubble_Sort' para organizar os elementos de forma crescente, dá esquerda para a direita, ela retona um ponteiro 
    * de inteiro que sera o vetor organizado.
    *   Criei a função 'rank_' para ranquear os elementos, ela retorna um ponteiro de inteiro, que é o vetor de ranques, está função só é 
    * eficaz para repetições de qualquer quantidade de elementos.
    *   Criei a função 'imprimir' para apenas imprimir o vetor que contém os elementos organizado crescentemente, o vetor de ranques estando 
    * ordenado, o vetor original e o vetor de ranques estando na ordem original de cada elemento.
*/

#include <iostream>

using namespace std;

int* bubble_Sort(int vet[], int tam){
    int n = tam;
    while ((n-1) > 0){
        int aux, ord = 0; 
        for (int i = 0; i < n; i++){
            if (vet[i] > vet[i+1]){
                ord = 1;
                aux = vet[i];
                vet[i] = vet[i+1];
                vet[i+1] = aux;
            }
        }
        if (ord == 0){
            break;
        }

        n -= 1;
    }

    cout << endl << endl;

    return vet;
}

float* rank_(int vetor[], int tam){
    float* vet_aux = new float(tam);

    for (int i = 0; i < tam; i++)
    {
        int cont = 1, aux = i, aux2 = 0;
        float rank = 0;

        if (i < tam-1)
        {
            if (vetor[aux] == vetor[aux+1])
            {
                while ((vetor[aux] == vetor[aux+1]) && (aux < tam-1))
                {
                    cont++;
                    aux++;
                }
                
                if (cont > 1)
                {
                    for (int j = 0; j < (cont); j++)
                    {
                        aux2 += i+j;
                    }
                }
                
                if (cont > 1)
                {   
                    rank = ((float) aux2/cont);
                    rank += 1.0;
                }
                
                for (int k = i; k < aux+1; k++)
                {
                    vet_aux[k] = rank;
                }
                
                i = aux;
        
            }else
            {
                vet_aux[i] = (float) i+1;
            }
                
        }else{
                 vet_aux[i] = i+1;
        }
        
    }
  
    return vet_aux;
}


void imprimir(int vetor_ordenado[], float rank[], int vetor_original[], int tam){

    cout << "Vetor ordenado:--------------------";
    for (int i = 0; i < tam; i++)
    {
        cout << " " << vetor_ordenado[i];
    }

    cout << endl << "Rank's de cada elemento ordenado:--";
    for (int i = 0; i < tam; i++)
    {
        cout << " " << rank[i];
    }

    cout << endl << "vetor original:--------------------";
    for (int i = 0; i < tam; i++)
    {
        cout << " " << vetor_original[i];
    }

    cout << endl << "Rank's de cada elemento original:--";
    for (int i = 0; i < tam; i++)   
    {
        for (int j = 0; j < tam; j++)   
        {
            if (vetor_original[i] == vetor_ordenado[j])
            {
                cout << " " << rank[j];
                break;
            }
                
        }
        
    }
}

int main(){

    int n;
    cout << "Digite o tamanho do vetor: ";
    cin >> n;
    
   int s[n], vet_original[n];
    
    for (int i = 0; i < n; i++){
    
    cout << "Digite os elemento " << i << " do vetor: ";
    cin >> s[i];
    }

    cout << endl << endl;
    cout << "Vetor inicialmente: ";
    for (int i = 0; i < n; i++){
        cout << " " << s[i];
        vet_original[i] = s[i];
    }
    cout << endl << endl;

    int* vetor = bubble_Sort (s, n);

    float* rank = rank_(vetor, n);
    imprimir(vetor, rank, vet_original, n);

    delete [] vetor; 
    delete [] rank;

    return 0;
}