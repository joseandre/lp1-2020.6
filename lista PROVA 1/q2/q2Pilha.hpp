#ifndef Q2PILHA_HPP
#define Q2PILHA_HPP

class Pilha {
	int capacidade;					//atributo que armazena a capacidade total do objeto
	int tamanho;					//atributo que armazena o tamanho atual (quantidade de elementos) do objeto
	int *elementos;					//atributo que armazena os elementos do objeto
	
	public:
	
	Pilha(int capacidade);			//contrutor
	~Pilha();						// destrutor
	void set_elemento(int valor);	//adiciona elementos ao topo da lista
	int get_Topo();					// retorna o topo
	int get_tamanho();				//retorna o tamanho
	void set_Tamanho();				//incrementa +1 no atributo 'tamanho'
	void remove_Tamanho();			//decrementa -1 no atributo 'tamanho'
	int get_capadicade();			//retorna a capacidade
	void get_elementos();			//imprime os elementos do atributo '*elementos'
	int* get_elementos_2_0();		//retorna o atributo '*elementos'
	int get_valor(int valor);		//procura e retorna o primeiro índice, do valor que foi passado por parâmetro
	int get_posicao(int pos);		//retorna o elemento que está no índice que foi passado por parâmetro
	void transferir(int pilha[]);	//método para transferir os elementos de uma lista para a outra
	void remover();					//remove o elemento do topo da lista
};
#endif