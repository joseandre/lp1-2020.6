#ifndef Q2FILA_HPP
#define Q2FILA_HPP
#include "q2Pilha.hpp"

class Fila {
    Pilha * pilha1;					//atributo que armazena um objeto pilha
    Pilha * pilha2;					//atributo que armazena um segundo objeto pilha
	
	public:
	
	Fila(int capacidade);			//construtor
	~Fila();						//destrutor
	void set_elemento(int valor);	//adiciona elementos ao final da lista
	int get_tamanho();				//retorna o tamanho da lista
	void get_elementos();			//imprime os elementos da lista
	int get_valor(int valor);		//procura e retorna o primeiro índice, do valor que foi passado por parâmetro
	int get_posicao(int pos);		//retorna o elemento que está no índice que foi passado por parâmetro
	void remover();					//remove o elemento que foi adicionado a mais tempo na lista
};
#endif