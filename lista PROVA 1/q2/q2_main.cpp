/*
	* 	Utilizei os arquivos que criei na lista 4 e fiz modificações nele para conseguir responder a questão 2.
	* 	Descrevi o que cada método faz, nos arquivos .hpp
	*	A 'pilha1', é a principal, botei para imprimir a segunda apenas para ver que ela está organizada no sentido contrário
	*	Eu utilizo duas pilhas em sentidos diferentes, fico alternando de uma pilha para a outra sempre que adicionar e remover.
	*
	* 	Me descupe não comentar direito, já são 23:44.

*/

#include <iostream>
#include "q2Fila.hpp"

using namespace std;

int main (){
	Fila fila(10);

	fila.set_elemento(1);  //adiciona um elemento na lista
	fila.set_elemento(2);
	fila.set_elemento(3);
	fila.set_elemento(5);
	fila.set_elemento(7);
	fila.set_elemento(11);



	cout << endl << "O valor está na posição: " << fila.get_valor(2) << endl; // busca o primeiro elemento igual o do parâmetro e diz qual é o índice dele

	cout << "Na posição tem o valor: " << fila.get_posicao(2) << endl;	// busca o índice e diz qual elemento tem dentro dele

	cout << endl << "O tamanho da fila é: " << fila.get_tamanho() << endl; // retorna o tamanho da lista

	cout << endl;

	fila.get_elementos(); //imprime os elementos

	fila.remover(); // remove o elemento que foi alocado à mais tempo

	cout << "O tamanho da fila é: " << fila.get_tamanho() << endl; // retorna o tamanho da lista
    
	cout << endl;

    fila.get_elementos(); //imprime os elementos

	fila.~Fila(); // destrutor da classe

	return 0;
}