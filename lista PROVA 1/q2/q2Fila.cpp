#include "q2Fila.hpp"
#include "q2Pilha.hpp"
#include <iostream>

using namespace std;

Fila::Fila(int capacidade){
    pilha1 = new Pilha(capacidade);
    pilha2 = new Pilha(capacidade);
}

Fila::~Fila() {
    pilha1->~Pilha();
    pilha2->~Pilha();
	delete [] pilha1;
    delete [] pilha2;
}

void Fila::set_elemento(int valor) {
	
	if (pilha1->get_tamanho() < pilha1->get_capadicade())
    {
        pilha1->set_elemento(valor);
        pilha2->set_Tamanho();
        pilha2->transferir(pilha1->get_elementos_2_0());
       
    }else{
        cout << "A fila está cheia!!" << endl;
    }
}

int Fila::get_tamanho(){
	return pilha1->get_tamanho();
}

void Fila::get_elementos(){
cout << "Pilha 1 :" <<	endl;
pilha1->get_elementos();

cout << "Pilha 2 :" << endl;
pilha2->get_elementos();
   
	cout << endl;
}

int Fila::get_valor(int valor){
	int index = -1;

    for (int i = 0; i < pilha1->get_tamanho(); i++) {
    	if (valor == pilha1->get_posicao(i)) {
            index = i;
            break;
        }
    }
    return index;
}

int Fila::get_posicao(int pos){
	return pilha1->get_posicao(pos);
}

void Fila::remover(){
	if (pilha2->get_tamanho() > 0){
        pilha2->remover();
        pilha1->remove_Tamanho();
        pilha1->transferir(pilha2->get_elementos_2_0());
    }else{
		cout << "Não existe elementos na lista!!" << endl;
	}
}
