#include "q1Fila.hpp"
#include <iostream>

using namespace std;

Fila::Fila(int capacidade) {
	this->capacidade = capacidade;
	fim = 0;
	ini = 0;
	tamanho = 0;
	elementos = new int[capacidade];
}

Fila::~Fila() {
	delete [] elementos;
}

void Fila::set_elemento(int valor) {
		

	if (fim == ini)
	{
		fim = 0;
		ini = 0;
	}
	
	if (fim <= capacidade)
	{
		elementos[fim] = valor;
		fim++;
		tamanho++;
	}else{
		cout << "A pilha está cheia!!" << endl;
	}
}

void Fila::set_Tamanho(){
	tamanho++;
	fim++;
}

void Fila::remove_Tamanho(){
	tamanho--;
	ini++;
}

int Fila::get_tamanho(){
	return tamanho;
}

int Fila::get_capacidade(){
	return capacidade;
}

void Fila::get_elementos(){
	cout << "Os elementos são:";
    for (int i = ini; i < fim; i++) {
    	cout << " " << elementos[i];
    }
	cout << endl;
}

int* Fila::get_elementos_2_0(){
	return elementos;
}

void Fila::transferir(int vetor[]){
	int j = fim-1;
	for (int i = ini; i < fim; i++)
	{
		elementos[i] = vetor[j];
		j--;
	}
}

int Fila::get_valor(int valor){
	int index = -1;

    for (int i = 0; i < fim; i++) {
    	if (valor == elementos[i]) {
            index = i;
            break;
        }
    }
    return index;
}

int Fila::get_primeiro_elemento(){
	return elementos[ini];
}

void Fila::remover(){
	if (fim != ini){
		elementos[ini] = -1;
		ini++;
		tamanho--;
	}else{
		cout << "Não existe elementos na lista!!" << endl;
	}
}
