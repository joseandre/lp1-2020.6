#ifndef Q1PILHA_HPP
#define Q1PILHA_HPP
#include "q1Fila.hpp"

class Pilha {

	Fila * fila1;					//atributo que armazena um objeto fila
	Fila * fila2;					//atributo que armazena um segundo objeto fila

	public:
	
	Pilha(int capacidade);			//construtor
	~Pilha();						//destrutor
	void set_elemento(int valor);	//adiciona elemento ao topo da lista
	int get_Topo();					//retorna o elemento do topo da lista
	int get_tamanho();				//retorna o tamanho da lisra
	void get_elementos();			//imprime os elementos da lista
	//int get_valor(int valor);		/*implementarei depois*/
	//int get_posicao(int pos);		/*implementarei depois*/
	void remover();					//remove o elemento do topo
};
#endif