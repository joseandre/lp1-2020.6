#include "q1Pilha.hpp"
#include "q1Fila.hpp"
#include <iostream>

using namespace std;

Pilha::Pilha(int capacidade) {
	fila1 = new Fila(capacidade);
	fila2 = new Fila(capacidade);
}

Pilha::~Pilha() {
	fila1->~Fila();
    fila2->~Fila();
	delete [] fila1;
    delete [] fila2;
}

void Pilha::set_elemento(int valor) {
	if (fila1->get_tamanho() <= fila1->get_capacidade())
	{
		fila1->set_elemento(valor);
		fila2->set_Tamanho();
		fila2->transferir(fila1->get_elementos_2_0());
	}else{
		cout << "A pilha está cheia!!" << endl;
	}
	
}

int Pilha::get_tamanho(){
	if (fila1->get_tamanho() > 0)
	{
	return fila1->get_tamanho();
	}else{
		return -1;
	}
	
}

int Pilha::get_Topo(){
	if (fila2->get_tamanho() > 0)
	{
	return fila2->get_primeiro_elemento();
	}else{
		return -1;
	}
}

void Pilha::get_elementos(){
	cout << "Fila 1 :" <<	endl;
	fila1->get_elementos();

	cout << "Fila 2 :" << endl;
	fila2->get_elementos();
   
	cout << endl;
}

/*int Pilha::get_valor(int valor){
	int aux = -1;

    for (int i = 0; i < fila1->get_tamanho(); i++) {
    	if (valor == elementos[i]) {
            aux = i;
            break;
        }
    }
    return aux;
}*/

/*int Pilha::get_posicao(int pos){
	return elementos[pos];
}*/

void Pilha::remover(){
	if (fila2->get_tamanho() > 0){
		fila2->remover();
		fila1->remove_Tamanho();
		fila1->transferir(fila2->get_elementos_2_0());
	}else{
		cout << "Não existe elementos na lista!!" << endl;
	}
}
