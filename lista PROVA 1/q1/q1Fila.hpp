#ifndef Q1FILA_HPP
#define Q1FILA_HPP

class Fila {
	int ini, fim, tamanho, capacidade;	//atributos do objeto fila: início, fim, tamanho e capacidade
	int * elementos;					//atributo que armazena os elementos do objeto
	
	public:
	
	Fila(int capacidade);				//construtor
	~Fila();							//destrutor
	void set_elemento(int valor);		//adiciona elemento ao fim da lista
	void set_Tamanho();					//incrementa +1 no atributo 'tamanho'
	void remove_Tamanho();				//decrementa -1 no atributo 'tamanho'
	int get_tamanho();					//retorna o atributo 'tamanho', que é o tamanho da lista
	int get_capacidade();				// retorna o atributo 'caapacidade', que é a capacidade total da lista
	void get_elementos();				//imprime os elementos da lista
	int* get_elementos_2_0();			//retorna o atributo '*elementos', que é onde estão armazenados os elementos da lista
	void transferir(int elementos[]);	//transfere os elementos de uma lista tipo fila, para outra
	int get_valor(int valor);			//procura se tem na lista o elemento que foi passado como parâmetro, se tiver, ele retorna o índice do primeiro que achar
	int get_primeiro_elemento(); 		//retorna o primeiro elemento da lista, ele só é utilizado na 'fila2', pois o inicio dela é o fim da 'fila1', assim, também é o topo, quando for chamada na classe 'Pilha'
	void remover();						//remove o elemento que foi adicionado a mais tempo na lista
};
#endif