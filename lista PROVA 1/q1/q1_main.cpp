/*
	* 	Utilizei os arquivos que criei na lista 4 e fiz modificações nele para conseguir responder a questão 2.
	* 	Descrevi o que cada método faz, nos arquivos .hpp
	* 	A 'fila1', é a principal, botei para imprimir a segunda apenas para ver que ela está organizada no sentido contrário
	*	Eu utilizo duas filas em sentidos diferentes, fico alternando de uma fila para a outra sempre que adicionar e remover.

	* 	Me descupe não comentar direito, já são 23:45.
*/

#include <iostream>
#include "q1Pilha.hpp"

using namespace std;

int main (){
	Pilha pilha(10);

	pilha.set_elemento(1); //adiciona um elemento na lista
	pilha.set_elemento(2);
	pilha.set_elemento(3);
	pilha.set_elemento(4);
	pilha.set_elemento(5);
	pilha.set_elemento(6);


	//cout << "O valor está na posição: " << pilha.get_valor(2) << endl; // busca o primeiro elemento igual o do parâmetro e diz qual é o índice dele

	//cout << "Na posição tem o valor: " << pilha.get_posicao(2) << endl;	// busca o índice e diz qual elemento tem dentro dele

	cout << "O tamanho da pilha é: " << pilha.get_tamanho() << endl; //retorna o tamanho da pilha

	cout << "O topo da pilha é: " << pilha.get_Topo() << endl; //retorna o topo da lista
	
	cout << endl;

	pilha.get_elementos(); //imprime os elementos

	pilha.remover(); // remove o elemento que está no topo da lista

	cout << "O tamanho da pilha é: " << pilha.get_tamanho() << endl; //retorna o tamanho da pilha
    
	cout << "O topo da pilha é: " << pilha.get_Topo() << endl; //retorna o topo da lista

	cout << endl;

    pilha.get_elementos(); //imprime os elementos

	pilha.~Pilha(); // destrutor da classe

	return 0;
}