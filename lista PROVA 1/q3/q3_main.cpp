/*
	*	Reutilizei os algoritmos de pilha e fila feitos na lista passada.
	*	Criei uma função tipo 'Fila*' para receber dois parâmetros do tipo 'Pilha' e retornar um objeto 'Fila*'. Esta função junta os 
	* elementos das duas pilhas de forma ordenada, com o menor elemento no extremo da esquerda da fila e o maior na direita.
	*
	* Me descupe não comentar direito, já são 23:43.
*/

#include "q3Pilha.hpp"
#include "q3Fila.hpp"
#include <iostream>

using namespace std;

//Função que junta os elemento das pilhas de forma ordenada em uma fila e a retorna.
Fila* ordenando(Pilha *pilha1, Pilha *pilha2){
	int i = 0, tam1 = pilha1->get_tamanho(), tam2 = pilha2->get_tamanho();
	Fila *fila = new Fila(pilha1->get_tamanho() + pilha2->get_tamanho());

	tam1--;
	tam2--;

	while (tam1 >= 0 || tam2 >= 0)
	{
		if (tam1 >= 0)
		{
			if (tam2 >= 0)
			{	
				if ((pilha1->get_posicao(tam1) < pilha2->get_posicao(tam2)) || (pilha1->get_posicao(tam1) == pilha2->get_posicao(tam2)))
				{
					fila->set_elemento(pilha1->get_posicao(tam1));
					tam1--;
					i++;
				}
			}else
			{
				fila->set_elemento(pilha1->get_posicao(tam1));
				tam1--;
				i++;
			}
			
		}

		if (tam2 >= 0)
		{
			if (tam1 >= 0)
			{
				if ((pilha2->get_posicao(tam2) < pilha1->get_posicao(tam1)) || (pilha2->get_posicao(tam2) < pilha1->get_posicao(tam1)))
				{
					fila->set_elemento(pilha2->get_posicao(tam2));
					tam2--;
					i++;
				}
			}else
			{
				fila->set_elemento(pilha2->get_posicao(tam2));
				tam2--;
				i++;
			}
			
		}
		
		
	}

	return fila;
}

int main (){

	Pilha* pilha_A;				//criando objeto 'pilha_A'
	pilha_A = new Pilha (6);	//alocando dinamicamente
	pilha_A->set_elemento(7); 	//adiciona um elemento na lista
	pilha_A->set_elemento(5);
	pilha_A->set_elemento(3);
	pilha_A->set_elemento(2);
	pilha_A->set_elemento(1);
	pilha_A->set_elemento(0);

	Pilha* pilha_B;				//criando objeto 'pilha_B'
	pilha_B = new Pilha (7);	//alocando dinamicamente
	pilha_B->set_elemento(12); 	//adiciona um elemento na lista
	pilha_B->set_elemento(10);
	pilha_B->set_elemento(8);
	pilha_B->set_elemento(5);
	pilha_B->set_elemento(4);
	pilha_B->set_elemento(2);
	pilha_B->set_elemento(1);
	
	Fila* fila;																	//criando objeto 'fila'
	fila = new Fila(pilha_A->get_tamanho() + pilha_B->get_tamanho());			//alocando dinamicamente, utilizando como capacidade a soma do tamanho da 'pilha1' e 'pilha2'

	fila = (ordenando(pilha_A, pilha_B));										//recebendo o retorno de uma fila, da função 'ordenando'

	fila->get_elementos();														//imprime os elementos do objeto

	cout << endl << endl << "TAMAMHNO FILA: " << fila->get_tamanho() << endl;	//retorna o tamanho do objeto

	pilha_A->~Pilha(); 		// destrutor da classe
	pilha_B->~Pilha(); 		// destrutor da classe

	fila->~Fila();			// destrutor da classe

	return 0;
}