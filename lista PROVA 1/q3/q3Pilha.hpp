#ifndef Q3PILHA_HPP
#define Q3PILHA_HPP

class Pilha {
	int capacidade;
	int tamanho;
	int * elementos;
	
	public:
	
	Pilha(int capacidade);
	~Pilha();
	void set_elemento(int valor);
	int get_Topo();
	int get_tamanho();
	void get_elementos();
	int* get_elementos_2_0();		//retorna o atributo '*elementos'
	int get_valor(int valor);
	int get_posicao(int pos);
	void remover();
};
#endif